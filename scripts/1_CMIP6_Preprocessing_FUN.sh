#!/bin/bash

###### HEADER - 1_CMIP6_Preprocessing_FUN.sh #################################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    25.06.2021
# Purpose: With this script, all CMIP6 data is pre-processed to be analysed
#          using R - FUNCTIONS.
##############################################################################

#### DEFINE FUNCTIONS ########################################################
## Copy grid files:
copy_grid_files () {
  mkdir -pv $wd_path/_helper_files/
  cp $grid_cmip6ng_orig $grid_cmip6ng
  cp $grid_cesm1le_orig $grid_cesm1le
  cp $grid_era5_processed $grid_era5
}

## Which variables should be analysed:
get_variable_tres_list () {
  if [ "$VARIABLES" == "" ]; then
    VARIABLES=$( grep ^Variables\| $model_info_file | cut -d"|" -f2 )
  fi
  
  VARIABLES_DAY=""
  VARIABLES_MON=""
  VARIABLES_FIX="areacella sftlf orog"
  for VAR in $VARIABLES
  do
    tres_in=$( grep ^$VAR\| $model_info_file | cut -d"|" -f2 )
    if [ "$tres_in" = "day" ]; then
      VARIABLES_DAY="$VARIABLES_DAY $VAR"
    else 
      VARIABLES_MON="$VARIABLES_MON $VAR"
    fi
  done
}

## Get gripoint size, orography and land fraction in 2.5° resolution:
get_fixed_var () {
  ## Get grid size:
  filepath_in=${path_cmip6ng_in}/areacella/fx/native/
  filename_in=areacella_fx_${mod_lname}_piControl_r1i${mod_setting}_native.nc
  file_in=$filepath_in$filename_in
  
  filepath_out=${wd_path}/areacella/fx/g025/
  filename_out=areacella_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  # if [[ -f $file_out && ! "$overwrite" == "TRUE" ]]; then return 0; fi
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
    ## Check whether input file exists in CMIP6ng-archive:
    if [[ ! -f $file_in ]]
      then
      echo "       File not found in CMIP6ng: $file_in"
    else
      ## Get grid size file (at 2.5° resolution)
      if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° grid size file from $file_in"; fi
      cdo -s remapcon2,$grid_cmip6ng $file_in $file_out
    fi
  fi
  
  ## Get land-fraction
  filepath_in=${path_cmip6ng_in}/sftlf/fx/native/
  filename_in=sftlf_fx_${mod_lname}_piControl_r1i${mod_setting}_native.nc
  file_in=$filepath_in$filename_in
  
  filepath_out=${wd_path}/sftlf/fx/g025/
  filename_out=sftlf_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
    ## Check whether input file exists in CMIP6ng-archive:
    if [[ ! -f $file_in ]]
      then
      echo "       File not found in CMIP6ng: $file_in"
    else
      ## Get land fraction file (at 2.5° resolution)
      if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° land-fraction file from $file_in"; fi
      cdo -s remapcon2,$grid_cmip6ng $file_in $file_out
    fi
  fi
  
  ## Get orography:
  filepath_in=${path_cmip6_in}/piControl/fx/orog/${mod_lname}/r1i${mod_setting}/gn/
  filename_in=orog_fx_${mod_lname}_piControl_r1i${mod_setting}_gn.nc
  file_in=$filepath_in$filename_in
  
  filepath_out=${wd_path}/orog/fx/g025/
  filename_out=orog_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
    ## Check whether input file exists in CMIP6ng-archive:
    if [[ ! -f $file_in ]]
      then
      echo "       File not found in CMIP6: $file_in"
    else
      ## Get land fraction file (at 2.5° resolution)
      if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° orography file from $file_in"; fi
      cdo -s remapcon2,$grid_cmip6ng $file_in $file_out
    fi
  fi
}


## Get gripoint size, orography and land fraction in 2.5° resolution:
get_fixed_var_CESM12 () {
  ## Get grid size:
  file_in_ls=$( ls ../CMIP6/areacella/fx/g025/areacella_fx_*.nc )
  
  filepath_out=${wd_path}/areacella/fx/g025/
  filename_out=areacella_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  if [[ ! -z "$file_in_ls" ]]; then
    file_in=$( echo $file_in_ls | cut -d" " -f1 )
    if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
      if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° grid size file from $file_in"; fi
      cp -rf $file_in $file_out
    fi
  else
    echo "       Run CMIP6 script first to obtain grid point size for CESM12!"
  fi
  
  ## Get land-fraction
  file_in=/home/beyerleu/cesm/USGS-gtopo30_1.9x2.5_remap_c050602.nc
  
  filepath_out=${wd_path}/sftlf/fx/g025/
  filename_out=sftlf_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  # if [[ -f $file_out && ! "$overwrite" == "TRUE" ]]; then return 0; fi
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
    if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° land-fraction file from $file_in"; fi
    cdo -s setname,sftlf, -remapcon2,$grid_cmip6ng, -mulc,100, -selvar,LANDFRAC $file_in $file_out
  fi
  
  ## Get orography:
  filepath_out=${wd_path}/orog/fx/g025/
  filename_out=orog_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  # if [[ -f $file_out && ! "$overwrite" == "TRUE" ]]; then return 0; fi
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
    if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° orography file from $file_in"; fi
    cdo -s setname,orog, -remapcon2,$grid_cmip6ng, -divc,9.81 -selvar,PHIS $file_in $file_out
  fi
}


## Get gripoint size, orography and land fraction in 2.5° resolution:
get_fixed_var_ERA5 () {
  ## Get grid size:
  file_in_ls=$( ls ../CMIP6/areacella/fx/g025/areacella_fx_*.nc )
  
  filepath_out=${wd_path}/areacella/fx/g025/
  filename_out=areacella_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  if [[ ! -z "$file_in_ls" ]]; then
    file_in=$( echo $file_in_ls | cut -d" " -f1 )
    if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
      if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° grid size file from $file_in"; fi
      cp -rf $file_in $file_out
    fi
  else
    echo "       Run CMIP6 script first to obtain grid point size for CESM12!"
  fi
  
  ## Get land-fraction
  file_in=$path_ERA5Land_in/0.25deg_lat-lon_time-invariant/processed/regrid/era5-land_recent.lsm.025deg.time-invariant.nc
  # file_in=$path_ERA5_in/0.25deg_lat-lon_time-invariant/processed/regrid/era5_deterministic_recent.lsm.025deg.time-invariant.nc
  
  filepath_out=${wd_path}/sftlf/fx/g025/
  filename_out=sftlf_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  # if [[ -f $file_out && ! "$overwrite" == "TRUE" ]]; then return 0; fi
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
    if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° land-fraction file from $file_in"; fi
    # cdo copy $file_in $file_out.f32
    cdo -b f32 copy $file_in $file_out.f32
    # cdo -s setname,sftlf, -remapcon2,$grid_cmip6ng, -mulc,100, -b 32 -selvar,lsm $file_out.f32 $file_out
    cdo -s setname,sftlf, -remapcon2,$grid_cmip6ng, -mulc,100 -selvar,lsm $file_out.f32 $file_out
    rm -rf $file_out.f32
  fi
  
  ## Get orography:
  file_in=$path_ERA5Land_in/0.25deg_lat-lon_time-invariant/processed/regrid/era5-land_recent.z.025deg.time-invariant.nc
  # file_in=$path_ERA5_in/0.25deg_lat-lon_time-invariant/processed/regrid/era5_deterministic_recent.z.025deg.time-invariant.nc
  
  filepath_out=${wd_path}/orog/fx/g025/
  filename_out=orog_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  # if [[ -f $file_out && ! "$overwrite" == "TRUE" ]]; then return 0; fi
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then 
    if [ "$verbose" == "TRUE" ]; then echo "       Get 2.5° orography file from $file_in"; fi
    cdo -b f32 copy $file_in $file_out.f32
    cdo -s setname,orog, -remapcon2,$grid_cmip6ng, -divc,9.81, -selvar,z $file_out.f32 $file_out
    rm -rf $file_out.f32
  fi
}

## Get weighted vertical sum of soil moisture according to config file, and
## further set all 0-values (in some model the values for the oceans) to 
## the missing values:
get_vertsum_weighted () {
  file_in=$1
  weights_lvl=$2
  levels=$( cdo -s showlevel $file_in  )
  
  if [ "$verbose" == "TRUE" ]; then echo "       Sum over soil layers $levels with weights $weights_lvl in: $file_in"; fi
  
  cdo -s mulc,0 -vertsum, $file_in $file_in.vertsum

  ## Iterate over levels
  ITER=0
  for weight_lvl in $weights_lvl
  do
    ITER=$(expr $ITER + 1)
    sellevel_i=$( echo $levels | cut -d" " -f$ITER )
    # echo $sellevel_i
    cdo -s mulc,$weight_lvl, -sellevel,$sellevel_i $file_in $file_in.lvl$ITER
    cdo -s add $file_in.vertsum $file_in.lvl$ITER $file_in.vertsum.$ITER
    mv -f $file_in.vertsum.$ITER $file_in.vertsum  > /dev/null 2>&1
    rm -rf $file_in.lvl$ITER
  done
  
  ## Set all 0-values to missing values:
  if [ "$verbose" == "TRUE" ]; then echo "       Set all zero values in mrsol to the missing value"; fi
  cdo -s setrtomiss,-1000,0.00001 $file_in.vertsum $file_in.vertsum.nozero
  mv  -f $file_in.vertsum.nozero $file_in > /dev/null 2>&1
  rm -rf $file_in.vertsum
}

## Get day runmean CMIP6ng data:
get_runmean_CMIP6ng () {
  # VAR=$1
  # tres_in=$2
  # mod_lname=$3
  # EXP=$4
  # ens_i=$5
  # mod_setting=$6
  # sres_in=$7
  
  ## Should running mean be calculated? Not in case of mrsol variable:
  if [ "$VAR" == "mrsol" ]; then
    nameext=""
    rmlen=1
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
    rmlen=$RUNMEAN_LEN
  fi

  ## Get input and output filenames:
  filepath_in=${path_cmip6ng_in}/${VAR}/${tres_in}/${sres_in}/
  filename_in=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_${sres_in}.nc
  file_in=$filepath_in$filename_in

  filepath_out=${wd_path}/${VAR}/${tres_in}/g025/
  filename_out=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.nc
  file_out=$filepath_out$filename_out
  
  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]
  then
    ## Check whether input file exists in CMIP6ng-archive:
    if [[ ! -f $file_in ]]
      then
      echo "       File not found in CMIP6ng: $file_in"
      get_runmean_CMIP6
    else
      ## Get running mean (if necessary with re-mapping):
      if [ "$sres_in" == "native" ]; then
        if [ "$verbose" == "TRUE" ]; then echo "       Get $rmlen-day runmean/remap file $file_in"; fi
        cdo -s runmean,$rmlen, -remapcon2,$grid_cmip6ng $file_in $file_out
      else 
        if [ "$verbose" == "TRUE" ]; then echo "       Get $rmlen-day runmean of file $file_in"; fi
        cdo -s runmean,$rmlen $file_in $file_out
      fi
    fi
    ## Sum over vertical layers for soil moisture variable (mrsol), as given in config file:
    if [[ "$VAR" == "mrsol" ]]; then
      get_vertsum_weighted "$file_out" "$mod_soil_weight"
      # cdo -s vertsum,$mod_soil_weight $file_out $file_out.soilsum
      # mv $file_out.soilsum $file_out
      # rm -rf $file_out.soilsum
    fi
  fi

  ## Get Field-year mean of piControl tas series
  if [[ "$EXP" == "piControl" ]]
  then
    get_seasmean "$file_out"
    get_dailyvar "$file_out"

    if [[ "$VAR" == "tas" ]]
    then
      filename_out_ydaymean=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.FLDYRMEAN.nc
      get_fldyrmean "$file_out" "$filepath_out" "$filename_out_ydaymean" "FALSE"
    fi
  fi
}

## Get day data from CMIP6 archive and 1) merge temporally, 2) remap to g025
## grid and 3) get running mean:
get_runmean_CMIP6 () {
  filepath_in=${path_cmip6_in}/${EXP}/${tres_CMIP6_in}/${VAR}/${mod_lname}/r${ens_i}i${mod_setting}/gn/
  filename_in=${VAR}_${tres_CMIP6_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_gn_*.nc
  file_in=$filepath_in$filename_in
  
  echo "       Concatenating and processing $(ls $filepath_in | wc -l) in CMIP6 archive"
  # file_in1=$filepath_in$( ls -t $filepath_in | head -1 )
  
  # if [[ ! -f $filepath_in$file_in1 ]]
  # then
  #   echo  "       File not found in CMIP6 archive: $file_in1"
  #   return 1
  # fi
  
  ## Apply CDO function:
  /usr/local/cdo-1.9.9/bin/cdo -s runmean,$rmlen, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $file_in ] $file_out
  echo  "       File found and processed in CMIP6 archive"
}

## Get day runmean CESM12-LE data:
get_runmean_CESM12 () {
  ## Should running mean be calculated? Not in case of mrsol variable:
  if [ "$VAR" == "mrsol" ]; then
    nameext=""
    rmlen=1
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
    rmlen=$RUNMEAN_LEN
  fi
  
  ## Get input filepath:
  filepath_in=/net/bio/climphys/fischeer/CMIP5/EXTREMES/CESM12-LE/
  if [[ "$ens_i" -le "20" ]]; then
    STARTYR=1850
  else
    STARTYR=1940
  fi

  ## Get input and output filenames:
  filepath_out=${wd_path}/${VAR}/${tres_in}/g025/
  filename_out=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.nc
  file_out=$filepath_out$filename_out
  
  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]
  then
    if [ "$verbose" == "TRUE" ]; then echo "       Create file $filename_out"; fi
    if [[ "$VAR" == "tas" && "$RUNMEAN_LEN" == "7" ]]; then
      file_in=$filepath_in/STATS/tas_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.RUNMEAN7.nc
      cdo -s setname,$VAR, -remapcon2,$grid_cmip6ng, -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "tas" && "$RUNMEAN_LEN" != "7" ]]; then
      file_in=$filepath_in/tas_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
      cdo -setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "zg500" || "$VAR" == "psl" ]]; then
      if [[ "$ens_i" -ge "3" && "$ens_i" -le "8" ]]; then
        file_in=/net/xenon/climphys/jzeder/Projects/nonstationary-extremes/data/alternative_CESM12_input/z500_psl_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
      else 
        file_in=$filepath_in/z500_psl_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
      fi
      cdo -s setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "pr" && "$RUNMEAN_LEN" == "7" ]]; then
      file_in=$filepath_in/STATS/pr_mm_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.RUNMEAN7.nc
      cdo -s setname,$VAR, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "pr" && "$RUNMEAN_LEN" != "7" ]]; then
      file_in=$filepath_in/pr_mm_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
      cdo -s setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "mrsol" ]]; then
      file_in=$filepath_in/land_monthly_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
      cdo -s setname,$VAR, -remapcon2,$grid_cmip6ng, -setrtomiss,-1000,0.00001, -vertsum, -sellevel,$mod_soil_selection, -selvar,$CESM12_name $file_in $file_out
    else
      echo "ERROR: A $VAR piControl file does not exist yet!"
      exit 1
    fi

  fi
}


## Get day runmean CESM12-LE data:
get_runmean_ERA5 () {
  ## Should running mean be calculated? Not in case of mrsol variable:
  if [ "$VAR" == "mrsol" ]; then
    nameext=""
    rmlen=1
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
    rmlen=$RUNMEAN_LEN
  fi
  
  ## Get input and output filenames:
  filepath_out=${wd_path}/${VAR}/${tres_in}/g025/
  filename_out=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.nc
  file_out=$filepath_out$filename_out
  file_landfrac=${wd_path}/sftlf/fx/g025/sftlf_fx_${mod_lname}_piControl_r1i1p1_g025.nc
  
  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]
  then
    if [ "$verbose" == "TRUE" ]; then echo "       Create file $filename_out"; fi
    if [[ "$VAR" == "tas" ]]; then
      if  [[ "$rmlen" == "1" ]]; then
        files_in=$path_ERA5_in/0.25deg_lat-lon_1h/processed/regrid_tmean1d/era5_deterministic_recent.t2m.025deg.1h.*.tmean1d.nc
        cdo -s -setname,$VAR, -selvar,$ERA5_name, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $files_in ] $file_out #.ERA5Determ
        files_in=$path_ERA5Land_in/0.25deg_lat-lon_1h/processed/regrid_tmean1d/era5-land_recent.t2m.025deg.1h.*.tmean1d.nc
        cdo -s -setname,$VAR, -selvar,$ERA5_name, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $files_in ] $file_out.ERA5Land
      else
        ## Check whether 1day files are already available (then use these for running mean):
        filename_out_1d=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.RUNMEAN1.nc
        if [[ -f $filepath_out$filename_out_1d ]]; then
          cdo -s -runmean,$rmlen, $filepath_out$filename_out_1d $file_out
        else 
          files_in=$path_ERA5_in/0.25deg_lat-lon_1h/processed/regrid_tmean1d/era5_deterministic_recent.t2m.025deg.1h.*.tmean1d.nc
          cdo -s -setname,$VAR, -runmean,$rmlen, -selvar,$ERA5_name, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $files_in ] $file_out #.ERA5Determ
        fi
        filename_out_1d=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.RUNMEAN1.nc.ERA5Land
        if [[ -f $filepath_out$filename_out_1d ]]; then
          cdo -s -runmean,$rmlen, $filepath_out$filename_out_1d $file_out.ERA5Land
        else
          files_in=$path_ERA5Land_in/0.25deg_lat-lon_1h/processed/regrid_tmean1d/era5-land_recent.t2m.025deg.1h.*.tmean1d.nc
          cdo -s -setname,$VAR, -runmean,$rmlen, -selvar,$ERA5_name, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $files_in ] $file_out.ERA5Land
        fi
      fi
      # files_in=$path_ERA5_in/0.25deg_lat-lon_1h/processed/regrid_tmean1d/era5_deterministic_recent.t2m.025deg.1h.*.tmean1d.nc
      # cdo -s -setname,$VAR, -runmean,$rmlen, -selvar,$ERA5_name, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $files_in ] $file_out
      # files_in=$path_ERA5Land_in/0.25deg_lat-lon_1h/processed/regrid_tmean1d/era5-land_recent.t2m.025deg.1h.*.tmean1d.nc
      # cdo -s -setname,$VAR, -runmean,$rmlen, -selvar,$ERA5_name, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $files_in ] $file_out.ERA5Land
    elif [[ "$VAR" == "zg500" ]]; then
      if  [[ "$rmlen" == "1" ]]; then
        files_in=$path_ERA5_in/0.25deg_lat-lon_1h/processed/regrid_tmean1d/era5_deterministic_recent.z.025deg.1h.*.tmean1d.nc
        cdo -s -setunit,m, -setname,$VAR, -divc,9.81, -sellevel,500, -selvar,$ERA5_name, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $files_in ] $file_out
      else
        ## Check whether 1day files are already available (then use these for running mean):
        filename_out_1d=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.RUNMEAN1.nc
        if [[ -f $filepath_out$filename_out_1d ]]; then
          cdo -s -runmean,$rmlen, $filepath_out$filename_out_1d $file_out
        else
          files_in=$path_ERA5_in/0.25deg_lat-lon_1h/processed/regrid_tmean1d/era5_deterministic_recent.z.025deg.1h.*.tmean1d.nc
          cdo -s -setunit,m, -setname,$VAR, -divc,9.81, -sellevel,500, -runmean,$rmlen, -selvar,$ERA5_name, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $files_in ] $file_out
        fi
      fi
    elif [[ "$VAR" == "mrsol" ]]; then
      file_in_1=$path_ERA5Land_in/0.25deg_lat-lon_1m/processed/regrid/era5-land_recent.swvl1.025deg.1m.*.nc
      file_in_2=$path_ERA5Land_in/0.25deg_lat-lon_1m/processed/regrid/era5-land_recent.swvl2.025deg.1m.*.nc
      # file_in_1=$path_ERA5_in/0.25deg_lat-lon_1m/processed/regrid/era5_deterministic_recent.swvl1.025deg.1m.*.nc
      # file_in_2=$path_ERA5_in/0.25deg_lat-lon_1m/processed/regrid/era5_deterministic_recent.swvl2.025deg.1m.*.nc
      cdo -s -b 32 -setname,$VAR, -selvar,${ERA5_name}1, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $file_in_1 ] $file_out.lvl1
      cdo -s -b 32 -setname,$VAR, -selvar,${ERA5_name}2, -mergetime, -apply,-remapcon2,$grid_cmip6ng [ $file_in_2 ] $file_out.lvl2
      cdo -s setunit,kg/m2, -mulc,100, -mulc,$mod_soil_weight, -add $file_out.lvl1 $file_out.lvl2 $file_out.nomask
      cdo -f nc -mul -expr,'sftlf=((sftlf>50.0))?1.0:sftlf/0.0' $file_landfrac $file_out.nomask $file_out
      rm -rf $file_out.lvl1 $file_out.lvl2 $file_out.nomask
    else
      echo "ERROR: A $VAR piControl file does not exist yet!"
      exit 1
    fi

  fi
  
  ## Get seasonal mean of all time series
  get_seasmean "$file_out"
  # get_dailyvar "$file_out"

  if [[ "$VAR" == "tas" ]]
  then
    filename_out_ydaymean=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.FLDYRMEAN.nc
    get_fldyrmean "$file_out" "$filepath_out" "$filename_out_ydaymean" "TRUE"
       
    ## Copy files to get naming correct for maxima index selection:
    filename_out_ENSMEAN=${VAR}_${tres_in}_${mod_lname}_${EXP}_ENSMEAN_g025${nameext}.FLDYRMEAN.nc
    file_out_ENSMEAN=${filepath_out}/_FLDYRMEAN/$filename_out_ENSMEAN
    if [[ ! -f $file_out_ENSMEAN || "$overwrite" == "TRUE" ]]; then
      cp ${filepath_out}/_FLDYRMEAN/$filename_out_ydaymean $file_out_ENSMEAN
    fi
  fi
}

## Get day runmean CESM12-LE piControl data:
get_runmean_CESM12_pictl () {
  ## Should running mean be calculated? Not in case of mrsol variable:
  if [ "$VAR" == "mrsol" ]; then
    nameext=""
    rmlen=1
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
    rmlen=$RUNMEAN_LEN
  fi

  ## Get input and output filenames:
  filepath_out=${wd_path}/${VAR}/${tres_in}/g025/
  filename_out=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.nc
  file_out=$filepath_out$filename_out
  
  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]
  then
    if [ "$verbose" == "TRUE" ]; then echo "       Create file $filename_out"; fi
    if [[ "$VAR" == "tas" && "$RUNMEAN_LEN" == "7" ]]; then
      file_in=/net/h2o/climphys/gessnerc/CESM122/daily/TREFHT/TREFHT_CESM12-LE_pictl_0500-5282.RUNMEAN7.nc
      cdo -s setname,$VAR, -remapcon2,$grid_cmip6ng, -delete,year=5282,5281,5280 -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "tas" && "$RUNMEAN_LEN" != "7" ]]; then
      file_in=/net/h2o/climphys/gessnerc/CESM122/daily/TREFHT/TREFHT_CESM12-LE_pictl_0500-5279.nc
      cdo -setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "zg500" && "$RUNMEAN_LEN" == "7" ]]; then
      file_in=/net/h2o/climphys/gessnerc/CESM122/daily/Z500/Z500_CESM12-LE_pictl_0500-5279.RUNMEAN7.nc
      cdo -s setname,$VAR, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "zg500" && "$RUNMEAN_LEN" != "7" ]]; then
      file_in=/net/h2o/climphys/gessnerc/CESM122/daily/Z500/Z500_CESM12-LE_pictl_0500-5279.nc
      cdo -s setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "psl" && "$RUNMEAN_LEN" == "7" ]]; then
      echo "ERROR: A PSL piControl file does not exist yet!"
      exit 1
      # file_in=/net/h2o/climphys/gessnerc/CESM122/daily/Z500/Z500_CESM12-LE_pictl_0500-5279.RUNMEAN7.nc
      # cdo -s setname,$VAR, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "psl" && "$RUNMEAN_LEN" != "7" ]]; then
      echo "ERROR: A PSL piControl file does not exist yet!"
      exit 1
      # file_in=/net/h2o/climphys/gessnerc/CESM122/daily/Z500/Z500_CESM12-LE_pictl_0500-5279.nc
      # cdo -s setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "pr" ]]; then
      file_in=/net/h2o/climphys/gessnerc/CESM122/daily/PRECIP/PRECIP_CESM12-LE_pictl_0500-5279.nc
      cdo -s setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "mrsol" ]]; then
      file_in=/net/h2o/climphys/gessnerc/CESM122/monthly/SOILLIQ/SOILLIQ_CESM12-LE_pictl_0500-5279.nc
      cdo -s setname,$VAR, -remapcon2,$grid_cmip6ng, -setrtomiss,-1000,0.00001, -vertsum, -sellevel,$mod_soil_selection, -selvar,$CESM12_name $file_in $file_out
    else
      echo "ERROR: A $VAR piControl file does not exist yet!"
      exit 1
    fi
  fi

  ## Get Field-year mean of piControl tas series
  get_seasmean "$file_out"
  get_dailyvar "$file_out"

  if [[ "$VAR" == "tas" ]]
  then
    filename_out_ydaymean=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.FLDYRMEAN.nc
    get_fldyrmean "$file_out" "$filepath_out" "$filename_out_ydaymean" "FALSE"
  fi
}

## Get ensemble mean for historical and SSP data
get_ensmean () {
  if [ "$VAR" == "mrsol" ]; then
    nameext=""
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
  fi

  if [[ "$MODEL" == "CESM12" ]]; then
    ens_sel="{1..20}"
  else
    ens_sel="*"
  fi 
  
  ## Get input and output filenames:
  filepath_in=${wd_path}/${VAR}/${tres_in}/g025/
  file_in=$( eval echo $filepath_in${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_sel}i${mod_setting}_g025${nameext}.nc )
  # file_in=$filepath_in$filename_in

  filepath_out=$filepath_in
  filename_out=${VAR}_${tres_in}_${mod_lname}_${EXP}_ENSMEAN_g025${nameext}.nc
  file_out=$filepath_out$filename_out
  
  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]
  then
    ## Check whether input ensemble files exist:
    if [[ $(ls $filepath_in | wc -l) == "0" ]]
    then
      echo "       No ensemble members found to calculate ENSMEAN: $file_in"
    else
      echo "       Get ENSMEAN of $VAR: $filename_out"
      cdo -s -O ensmean $file_in $file_out
    fi
  fi

  ## Get seasonal means:
  get_seasmean "$file_out"

  ## Get yearday-ensemble-mean of tas
  if [ "$VAR" = "tas" ]
  then
    filename_out_ydaymean=${VAR}_${tres_in}_${mod_lname}_${EXP}_ENSMEAN_g025${nameext}.FLDYRMEAN.nc
    # file_out_ydaymean=$filepath_out$filename_out_ydaymean
    get_fldyrmean "$file_out" "$filepath_out" "$filename_out_ydaymean" "TRUE"
  fi
}

## Get seasonal mean for DJF / JJA
get_seasmean() {
  file_in=$1
  filename_in=$( basename $file_in )
  filepath_out=$( dirname $file_in )
  mkdir -p ${filepath_out}/_SEASMEAN/
  file_out_JJA=${filepath_out}/_SEASMEAN/${filename_in::-3}.JJAMEAN.nc
  file_out_DJF=${filepath_out}/_SEASMEAN/${filename_in::-3}.DJFMEAN.nc
  
  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out_JJA || ! -f $file_out_DJF || "$overwrite" == "TRUE" ]]
  then
    ## Check whether input ensemble files exist:
    echo "       Get JJAMEAN/DJFMEAN of: $file_in"
    cdo -s seasmean, -selseas,DJF, $file_in $file_out_DJF
    cdo -s seasmean, -selseas,JJA, $file_in $file_out_JJA
  fi
}


## Get daily variability (unit: stddev) for DJF / JJA in piControl runs:
get_dailyvar () {
  file_in=$1
  filename_in=$( basename $file_in )
  filepath_out=$( dirname $file_in )
  mkdir -p ${filepath_out}/_DAILYVAR/
  # file_out_JJA_DETR=${filepath_out}/_DAILYVAR/${filename_in::-3}.JJADETREND
  # file_out_DJF_DETR=${filepath_out}/_DAILYVAR/${filename_in::-3}.DJFDETREND
  file_out_DETR=${filepath_out}/_DAILYVAR/${filename_in::-3}.DETREND
  
  file_out_JJA=${filepath_out}/_DAILYVAR/${filename_in::-3}.JJADAILYVAR.nc
  file_out_DJF=${filepath_out}/_DAILYVAR/${filename_in::-3}.DJFDAILYVAR.nc
  
  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out_JJA || ! -f $file_out_DJF || "$overwrite" == "TRUE" ]]
  then
    ## Check whether input ensemble files exist:
    echo "       Get JJA/DJF daily variability of: $file_in"
    # cdo -s detrend,false, -selseas,DJF, $file_in $file_out_DJF_DETR
    # cdo -s detrend,false, -selseas,JJA, $file_in $file_out_JJA_DETR
    
    cdo -s trend,false,   $file_in $file_out_DETR.BETA0.nc $file_out_DETR.BETA1.nc
    cdo -s subtrend,false $file_in $file_out_DETR.BETA0.nc $file_out_DETR.BETA1.nc $file_out_DETR.nc
    
    cdo -s timstd, -selseas,DJF, -yseassub $file_out_DETR.nc -yseasmean, $file_out_DETR.nc $file_out_DJF
    cdo -s timstd, -selseas,JJA, -yseassub $file_out_DETR.nc -yseasmean, $file_out_DETR.nc $file_out_JJA
  fi
}

## Get annual global mean
get_fldyrmean() {
  file_in=$1
  filepath_out=$2
  filename_out=$3
  apply_smooth=$4
  mkdir -p ${filepath_out}/_FLDYRMEAN/
  file_out_fldyrmean=${filepath_out}/_FLDYRMEAN/${filename_out}
  
  if [[ ! -f $file_out_fldyrmean || "$overwrite" == "TRUE" ]]
  then
    echo "       Get FLDYRMEAN of ENSMEAN/piControl: $file_in"
    file_gridsize=${wd_path}/areacella/fx/g025/areacella_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
    
    cdo -s fldmean, -yearmean, -setgridarea,$file_gridsize, $file_in $file_out_fldyrmean
  fi
  
  if [ $apply_smooth = "TRUE" ]; then
    smooth_tas_ensmean "$file_out_fldyrmean" "$filepath_out" "$filename_out"
  fi
}

## Use R to smooth ensemble/spatial/annual means of tas:
## (the fourth argument are standard deviations of SSP/historical ensemble
## differences in tas, which should be reached by piControl smoothing)
smooth_tas_ensmean () {
  file_in=$1
  filepath_out=$2
  filename_out=$3
  sd_vec=$4
  
  nc_vars=$( cdo -s showname $file_in )
  if echo "$nc_vars" | grep -q "tas_smooth"; then
  # if [[ "tas_smooth" == *"$nc_vars"* ]]; then
    # echo "       Smooth tas-time series already exists for file: $file_in"
    return 0
  else
    echo "       Obtain smooth tas time series for file: $file_in"
    plotfile_out=$filepath_out/_checkplot/${filename_out::-3}.png
    echo $smooth_tas_ensmean_script
    echo $file_in
    echo $sd_vec
    /usr/local/R-4.0.3-openblas/bin/Rscript "$smooth_tas_ensmean_script" "$file_in" "$plotfile_out" "$sd_vec"
  fi
}


## ================= FUNCTIONS FOR SECOND PART OF SCRIPT =======================

## Use R to smooth ensemble/spatial/annual means of tas:
get_smooth_pictl () {
  filepath_in=${wd_path}/${VAR}/day/g025/
  filepath_fldyrmean=${filepath_in}/_FLDYRMEAN/

  if [[ "$MODEL" == "CESM12" ]]; then
    file_in_list="$( ls ${filepath_in}${VAR}_${tres_in}_${mod_lname}_historical_r{1..20}i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.nc )"
  else
    file_in_list="$( ls ${filepath_in}${VAR}_${tres_in}_${mod_lname}_ssp*_r*i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.nc ) $( ls ${filepath_in}${VAR}_${tres_in}_${mod_lname}_hist*_r*i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.nc )"
  fi 
  
  echo "    Getting difference for $( ls $file_in_list | wc -l) in FLDYRMEAN tas (orig/smooth)"
  sd_list=""
  for FILE in $file_in_list
  do
    filename=$(basename ${FILE::-3})
    fileEXP=$( echo $filename | cut -d"_" -f4 )
    file_fldyrmean_ensmean=${filepath_fldyrmean}/${VAR}_${tres_in}_${mod_lname}_${fileEXP}_ENSMEAN_g025*.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
    
    filename_fldyrmean=$filename.FLDYRMEAN.nc
    filename_fldyrmean_diffstd=$filename.FLDYRMEAN.diffstd.nc
    get_fldyrmean "$FILE" "$filepath_in" "$filename_fldyrmean" "FALSE"
    if [[ "$MODEL" == "CESM12" ]]; then
      cdo -s selvar,tas_smooth -timstd, -sub, -selvar,tas_smooth $file_fldyrmean_ensmean ${filepath_fldyrmean}/$filename_fldyrmean ${filepath_fldyrmean}$filename_fldyrmean_diffstd
      sd_new=$( ncdump ${filepath_fldyrmean}/$filename_fldyrmean_diffstd | tail -n2 | head -n1 | awk '{ print $3 }' )
    else
      filename_fldyrmean=$filename.FLDYRMEAN.nc
      filename_fldyrmean_diffstd=$filename.FLDYRMEAN.diffstd.nc
      get_fldyrmean "$FILE" "$filepath_in" "$filename_fldyrmean" "FALSE"
      cdo -s timstd, -sub, -selvar,tas_smooth $file_fldyrmean_ensmean ${filepath_fldyrmean}/$filename_fldyrmean ${filepath_fldyrmean}$filename_fldyrmean_diffstd.temp
      var_name="$( cdo -s showname ${filepath_fldyrmean}$filename_fldyrmean_diffstd.temp )"
      if [[ "$var_name" == *"tas_smooth"* && "$( echo $var_name | wc -w )" == "1" ]]; then
        cdo -s selvar,tas_smooth ${filepath_fldyrmean}$filename_fldyrmean_diffstd.temp ${filepath_fldyrmean}$filename_fldyrmean_diffstd
      else
        cdo -s selvar,tas        ${filepath_fldyrmean}$filename_fldyrmean_diffstd.temp ${filepath_fldyrmean}$filename_fldyrmean_diffstd
      fi
      rm -rf ${filepath_fldyrmean}$filename_fldyrmean_diffstd.temp
      sd_new=$( ncdump ${filepath_fldyrmean}/$filename_fldyrmean_diffstd | tail -n2 | head -n1 | awk '{ print $3 }' )
    fi 
    
    # echo $sd_new
    rm -rf ${filepath_fldyrmean}/$filename_fldyrmean_diffstd
    sd_ls="$sd_ls $sd_new"
  done
  
  ## Get smooth piControl data:
  filename_pictl=${VAR}_${tres_in}_${mod_lname}_piControl_r1i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
  file_pictl=${filepath_fldyrmean}${filename_pictl}
  
  var_exist=$( cdo -s showname $file_pictl )
  SUB='tas_smooth'
  if [[ "$var_exist" != *"$SUB"* ]]; then
    echo "    Smooth piControl file"
    smooth_tas_ensmean "$file_pictl" "${filepath_in}" "${filename_pictl}" "${sd_ls}"
  fi
  
  ## Get mean of smoothed temperature (anomaly reference):
  filename_pictl_timmean=${VAR}_${tres_in}_${mod_lname}_piControl_r1i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.TIMMEAN.nc
  file_pictl_timmean=${filepath_fldyrmean}${filename_pictl_timmean}
  if [[ ! -f $file_pictl_timmean || "$overwrite" == "TRUE" ]]; then cdo -s timmean $file_pictl $file_pictl_timmean; fi
}

## Get seasonal (JJA/DJF) tas basestate from pictl:
get_seas_basestate_tas() {
  path_tas_fldyrmean_pictl=${wd_path}/tas/day/g025/_FLDYRMEAN/
  file_tas_fldyrmean_pictl=${VAR}_${tres_in}_${mod_lname}_piControl_r1i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
  file_fldyrmean=$path_tas_fldyrmean_pictl$file_tas_fldyrmean_pictl
  
  path_tas_seasmean_pictl=${wd_path}/tas/day/g025/_SEASMEAN/
  file_tas_seasmean_pictl_JJA=${VAR}_${tres_in}_${mod_lname}_piControl_r1i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.JJAMEAN.nc
  file_tas_seasmean_pictl_DJF=${VAR}_${tres_in}_${mod_lname}_piControl_r1i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.DJFMEAN.nc
  file_JJA=$path_tas_seasmean_pictl$file_tas_seasmean_pictl_JJA
  file_DJF=$path_tas_seasmean_pictl$file_tas_seasmean_pictl_DJF
  
  SUB='tas_beta0'
  var_exist=$( cdo -s showname $file_JJA )
  if [[ "$var_exist" != *"$SUB"* ]]; then
    echo "    Get basestate of JJA tas"
    /usr/local/R-4.0.3-openblas/bin/Rscript $seas_basestate_tas_script "$file_fldyrmean" "$file_JJA"
  fi
  var_exist=$( cdo -s showname $file_DJF )
  if [[ "$var_exist" != *"$SUB"* ]]; then
    echo "    Get basestate of DJF tas"
    /usr/local/R-4.0.3-openblas/bin/Rscript $seas_basestate_tas_script "$file_fldyrmean" "$file_DJF"
  fi
}

## Get seasonal (JJA/DJF) scaling pattern with GMST:
get_seasonal_scaling_pattern() {
  if [ "$VAR" == "mrsol" ]; then
    nameext=""
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
  fi
  
  for SEAS in DJF JJA
  do
    ## Get input filenames for the variable of interest:
    filepath_in_var=${wd_path}/${VAR}/${tres_in}/g025/_SEASMEAN/
    filename_in_var_histssp=${VAR}_${tres_in}_${mod_lname}_*_ENSMEAN_g025${nameext}.${SEAS}MEAN.nc
    filename_in_var_picntrl=${VAR}_${tres_in}_${mod_lname}_piControl_r1i${mod_setting}_g025${nameext}.${SEAS}MEAN.nc
    file_in_var_list="$filepath_in_var$filename_in_var_picntrl $( ls $filepath_in_var$filename_in_var_histssp)"
    
    # echo $file_in_var_list
    ## Get input filenames of temperature covariate:
    filepath_in_tas=${wd_path}/tas/day/g025/_FLDYRMEAN/
    filename_in_tas_histssp=tas_day_${mod_lname}_*_ENSMEAN_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
    filename_in_tas_picntrl=tas_day_${mod_lname}_piControl_r1i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
    file_in_tas_list="$filepath_in_tas$filename_in_tas_picntrl $( ls $filepath_in_tas$filename_in_tas_histssp)"
    
    ## Run R-script to get scaling and smooth seasonal references
    /usr/local/R-4.0.3-openblas/bin/Rscript $seas_scaling_script "$file_in_tas_list" "$file_in_var_list" "$model_info_file" "$overwrite"
  done
}


## Get seasonal (JJA/DJF) scaling pattern with GMST:
get_seasonal_scaling_pattern_reanalysis() {
  if [ "$VAR" == "mrsol" ]; then
    nameext=""
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
  fi
  
  for SEAS in DJF JJA
  do
    ## Get input filenames for the variable of interest:
    filepath_in_var=${wd_path}/${VAR}/${tres_in}/g025/_SEASMEAN/
    filename_in_var_reanalysis=${VAR}_${tres_in}_${mod_lname}_reanalysis_r1i${mod_setting}_g025${nameext}.${SEAS}MEAN.nc
    file_in_var_list="$filepath_in_var$filename_in_var_reanalysis"
    
    ## Get input filenames of temperature covariate:
    filepath_in_tas=${wd_path}/tas/day/g025/_FLDYRMEAN/
    filename_in_tas_reanalysis=tas_day_${mod_lname}_reanalysis_ENSMEAN_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
    file_in_tas_list="$filepath_in_tas$filename_in_tas_reanalysis"
    
    ## Run R-script to get scaling and smooth seasonal references
    /usr/local/R-4.0.3-openblas/bin/Rscript $seas_scaling_script "$file_in_tas_list" "$file_in_var_list" "$model_info_file" "$overwrite"
    
    ## Copy files to get naming correct for maxima index selection:
    filename_out=${VAR}_${tres_in}_${mod_lname}_reanalysis_r1i${mod_setting}_g025${nameext}.${SEAS}MEAN.TREND.nc
    filename_out_ENSMEAN=${VAR}_${tres_in}_${mod_lname}_reanalysis_ENSMEAN_g025${nameext}.${SEAS}MEAN.TREND.nc
    file_out_ENSMEAN=${filepath_in_var}_TREND/$filename_out_ENSMEAN
    if [[ ! -f $file_out_ENSMEAN || "$overwrite" == "TRUE" ]]; then
      cp ${filepath_in_var}_TREND/$filename_out $file_out_ENSMEAN
    fi
    
    ## Subtract seasonal trends and calculate daily variability:
    file_in_STD=${wd_path}/${VAR}/${tres_in}/g025/${VAR}_${tres_in}_${mod_lname}_reanalysis_r1i${mod_setting}_g025${nameext}.nc
    filepath_out_STD=${wd_path}/${VAR}/${tres_in}/g025/_DAILYVAR/
    filename_out_STD=${VAR}_${tres_in}_${mod_lname}_piControl_r1i${mod_setting}_g025${nameext}.${SEAS}DAILYVAR.nc
    file_out_STD=$filepath_out_STD$filename_out_STD
    mkdir -pv $filepath_out_STD
    
    if [[ ! -f $file_out_STD || "$overwrite" == "TRUE" ]]; then
      if [ "$verbose" == "TRUE" ]; then echo "       Get daily variability file: $filename_out_STD"; fi
      # cdo -s -yearmean, -selvar,${VAR}_trend  $file_out_ENSMEAN $file_out_STD.trend
      cdo -s timstd, -yearsub -selseas,${SEAS}, $file_in_STD -yearmean, -selvar,${VAR}_trend $file_out_ENSMEAN $file_out_STD
      # cdo -s timstd, -yearsub -selseas,${SEAS}, $file_in_STD -selseas,${SEAS}, -sub -selvar,${VAR} $file_out_ENSMEAN -selvar,${VAR}_trend $file_out_ENSMEAN $file_out_STD
      # cdo timstd, -yseassub -selseas,${SEAS}, -sub -selvar,${VAR} $file_out_ENSMEAN -selvar,${VAR}_trend $file_out_ENSMEAN -selseas,${SEAS}, $file_in_STD $file_out_STD
    fi
    
  done
}


# for SEAS in DJF JJA
# do
#   ## Get input filenames for the variable of interest:
#   filepath_in_var=${wd_path}/${VAR}/${tres_in}/g025/_SEASMEAN/
#   # filename_in_var_histssp=${VAR}_${tres_in}_${mod_lname}_*_ENSMEAN_g025${nameext}.${SEAS}MEAN.nc
#   # file_in_var_histssp=$( eval echo $filepath_in_var${VAR}_${tres_in}_${mod_lname}_*r${ens_sel}i*_ENSMEAN_g025${nameext}.${SEAS}MEAN.nc )
#   filename_in_var_picntrl=${VAR}_${tres_in}_${mod_lname}_piControl_r1i${mod_setting}_g025${nameext}.${SEAS}MEAN.nc
#   
#   if [[ "$MODEL" == "CESM12" ]]; then
#     # file_in_list="{1..20}"
#     file_in_var_histssp=$( eval echo $filepath_in_var${VAR}_${tres_in}_${mod_lname}_historical_r${ens_sel}i${mod_setting}_ENSMEAN_g025${nameext}.${SEAS}MEAN.nc )
#     file_in_var_list="$filepath_in_var$filename_in_var_picntrl $file_in_var_histssp"
#   else
#     # ens_sel="*"
#     filename_in_var_histssp=${VAR}_${tres_in}_${mod_lname}_*_ENSMEAN_g025${nameext}.${SEAS}MEAN.nc
#     file_in_var_list="$filepath_in_var$filename_in_var_picntrl $( ls $filepath_in_var$filename_in_var_histssp )"
#   fi 
#   
#   # file_in_var_list="$filepath_in_var$filename_in_var_picntrl $( ls $filepath_in_var$filename_in_var_histssp )"
#   
#   # echo $file_in_var_list
#   ## Get input filenames of temperature covariate:
#   filepath_in_tas=${wd_path}/tas/day/g025/_FLDYRMEAN/
#   filename_in_tas_histssp=tas_day_${mod_lname}_*_ENSMEAN_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
#   filename_in_tas_picntrl=tas_day_${mod_lname}_piControl_r1i${mod_setting}_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
#   file_in_tas_list="$filepath_in_tas$filename_in_tas_picntrl $( ls $filepath_in_tas$filename_in_tas_histssp)"
#   # echo "$file_in_tas_list"
#   # echo
#   ## Run R-script to get scaling and smooth seasonal references
#   /usr/local/R-4.0.3-openblas/bin/Rscript $seas_scaling_script "$file_in_tas_list" "$file_in_var_list" "$model_info_file" "$overwrite"
#   # echo
#   # echo
# done




