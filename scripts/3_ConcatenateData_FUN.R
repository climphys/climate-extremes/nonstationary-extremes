#!/bin/Rscript

###### HEADER - 3_ConcatenateData_FUN.R #####################################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    20.09.2021
# Purpose: Reading in NetCDF data and compile them into two R-objects, one
#          list with 3D-fields (as data.frames) and one with a large 1D
#          data.frame (FUNCTIONS).
##############################################################################

#### FUNCTIONS ###############################################################
read_config_file_model <- function(model, conf_file_path) {
  ens_prelim <- system(paste0(r"{echo $( grep ^'}",model,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f5 )}"), intern=T)
  if(substr(ens_prelim,1,1)=="{") {
    ENS_mod <- as.integer(strsplit(system(paste("echo",ens_prelim), intern=T)," ")[[1]])
  } else {
    ENS_mod <- as.integer(strsplit(ens_prelim," ")[[1]])
  }
  
  ls_res <- list(mod_sname  =model,
                 mod_lname  =system(paste0(r"{echo $( grep ^'}",model,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f2 )}"), intern=T),
                 mod_setting=system(paste0(r"{echo $( grep ^'}",model,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f3 )}"), intern=T),
                 EXP        =strsplit(system(paste0(r"{echo $( grep ^'}",model,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f4 )}"), intern=T)," ")[[1]],
                 ENS        =ENS_mod)
  return(ls_res)
}

read_config_file_variables <- function(variable, conf_file_path) {
  df_res <- data.frame(var =variable,
                       tres=system(paste0(r"{echo $( grep ^'}",variable,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f2 )}"), intern=T))
  return(df_res)
}

read_config_file_locations <- function(location, conf_file_path) {
  df_res <- data.frame(loc_sname =location,
                       loc_lname =system(paste0(r"{echo $( grep ^'}",location,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f2 )}"), intern=T),
                       loc_lonE  =as.numeric(system(paste0(r"{echo $( grep ^'}",location,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f3 )}"), intern=T)),
                       loc_lonW  =as.numeric(system(paste0(r"{echo $( grep ^'}",location,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f4 )}"), intern=T)),
                       loc_latS  =as.numeric(system(paste0(r"{echo $( grep ^'}",location,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f5 )}"), intern=T)),
                       loc_latN  =as.numeric(system(paste0(r"{echo $( grep ^'}",location,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f6 )}"), intern=T)),
                       max_seas  =system(paste0(r"{echo $( grep ^'}",location,r"{'\| }",conf_file_path,r"{ | cut -d'|' -f7 )}"), intern=T))
  
  df_res$loc_lonE360 <- df_res$loc_lonE
  df_res$loc_lonW360 <- df_res$loc_lonW
  df_res$loc_lonE360[which(df_res$loc_lonE360<0)] <- df_res$loc_lonE360[which(df_res$loc_lonE360<0)]+360
  df_res$loc_lonW360[which(df_res$loc_lonW360<0)] <- df_res$loc_lonW360[which(df_res$loc_lonW360<0)]+360
  
  df_res$loc_lonCent360 <- df_res$loc_lonE360 + abs(df_res$loc_lonE-df_res$loc_lonW)/2
  df_res$loc_lonCent    <- df_res$loc_lonE    + abs(df_res$loc_lonE-df_res$loc_lonW)/2
  df_res$loc_latCent    <- df_res$loc_latS    + abs(df_res$loc_latN-df_res$loc_latS)/2
  return(df_res)
}

read_config_file <- function(conf_file_path) {
  models    <- strsplit(system(paste0(r"{echo $( grep ^'Models'\| }",conf_file_path,r"{ | cut -d'|' -f2 )}"), intern=T)," ")[[1]]
  locations <- strsplit(system(paste0(r"{echo $( grep ^'Locations'\| }",conf_file_path,r"{ | cut -d'|' -f2 )}"), intern=T)," ")[[1]]
  variables <- strsplit(system(paste0(r"{echo $( grep ^'Variables'\| }",conf_file_path,r"{ | cut -d'|' -f2 )}"), intern=T)," ")[[1]]
  
  models_ls <- list()
  for(model in models) {
    models_ls[[model]] <- read_config_file_model(model, conf_file_path)
  }
  
  res_ls <- list(
    model_ls    =models_ls,
    variables_df=do.call(rbind, lapply(variables, FUN=read_config_file_variables, conf_file_path)),
    location_df =do.call(rbind, lapply(locations, FUN=read_config_file_locations, conf_file_path))
  )
}

## Get full year:
get_4char_year <- function(date_str) {
  char_vec <- strsplit(date_str,"-")[[1]]
  char_str <- sprintf("%04i-%s-%s",as.integer(char_vec[1]),char_vec[2],char_vec[3])
  return(char_str)
}

read_1d_nc  <- function(exp_ens_path, model_sname, loc_sname, max_seas, max_index) {
  ens  <- as.integer(exp_ens_path[1])
  exp  <- exp_ens_path[2]
  path <- exp_ens_path[3]
  # read_1d_nc  <- function(ens, exp, path, model_sname, loc_sname, max_seas) {
  #   ens  <- as.integer(ens)
  # browser()
  nc_file_TxIdx  <- nc_open(path)
  
  time_TxIdx <- nc.get.time.series(nc_file_TxIdx)
  # year_TxIdx <- as.integer(substr(time_TxIdx,1,4))
  year_TxIdx <- as.integer(as.character(time_TxIdx, format="%Y"))
  doy_TxIdx  <- (ncvar_get(nc_file_TxIdx, paste0(max_index,"_IDX")))
  
  if(max_seas=="DJF") {
    doy_TxIdx  <- doy_TxIdx - 180
  }
  
  ## Get centered DOY (Jan 15 for DJF and July 15 for JJA)
  if(max_seas=="DJF") {
    doy_cen <- doy_TxIdx-15
  } else {
    doy_cen <- doy_TxIdx-195
  }
  
  ## Add first and second harmonic of DOY data:
  DOY_cen_sin1 <- sin(doy_cen*2*pi/365)
  DOY_cen_cos1 <- cos(doy_cen*2*pi/365)
  DOY_cen_sin2 <- sin(doy_cen*4*pi/365)
  DOY_cen_cos2 <- cos(doy_cen*4*pi/365)
  
  ## Get date of occurrences:
  year_TxIdx[which(doy_TxIdx<0)] <- year_TxIdx[which(doy_TxIdx <0)] - 1
  doy_TxIdx[which(doy_TxIdx<=0)] <-  doy_TxIdx[which(doy_TxIdx<=0)] + 365
  date_TxIdx_orig     <- paste(year_TxIdx,
                               substr(as.Date("2001-01-01")+(doy_TxIdx-1), 6,10),
                               sep="-")
  date_TxIdx_orig <- sapply(date_TxIdx_orig, get_4char_year)
  
  ## Get variables:
  var_vec <- names(nc_file_TxIdx$var)
  covar_vec <- sort(var_vec[c(grep("ABS",var_vec),
                              grep("ANO",var_vec),
                              grep("STD",var_vec))])
  
  ## Put data into data.frame:
  res_df <- data.frame(
    model   =model_sname,
    ens     =ens,
    exp     =exp,
    location=loc_sname,
    max_seas=max_seas,
    
    Tx7d    =(ncvar_get(nc_file_TxIdx, max_index)),
    date    =date_TxIdx_orig,
    year    =as.integer(substr(date_TxIdx_orig,1,4)),
    month   =as.integer(substr(date_TxIdx_orig,6,7)),
    day     =as.integer(substr(date_TxIdx_orig,9,10)),
    DOY     =doy_TxIdx,
    DOY_cen =doy_cen,
    DOY_cen_cos1 =DOY_cen_cos1,
    DOY_cen_sin1 =DOY_cen_sin1,
    DOY_cen_cos2 =DOY_cen_cos2,
    DOY_cen_sin2 =DOY_cen_sin2,
    GMST         =(ncvar_get(nc_file_TxIdx, "gmst")),
    GMST_smooth  =(ncvar_get(nc_file_TxIdx, "gmst_smooth")),
    row.names = NULL
  )
  for(covar in covar_vec) {
    if(length(dim((ncvar_get(nc_file_TxIdx, covar))))==1)
      res_df[,covar] <- (ncvar_get(nc_file_TxIdx, covar))
  }
  nc_close(nc_file_TxIdx)
  
  
  if(max_seas=="DJF") {
    res_df <- res_df[-c(1,nrow(res_df)),]
  }
  return(res_df)
}


read_3d_nc  <- function(exp_ens_path, model_sname, loc_sname, max_seas, max_index) {
  ens  <- as.integer(exp_ens_path[1])
  exp  <- exp_ens_path[2]
  path <- exp_ens_path[3]
  
  ## Open nc file and get time information:
  nc_file_TxIdx  <- nc_open(path)
  time_TxIdx <- nc.get.time.series(nc_file_TxIdx)
  year_TxIdx <- as.integer(as.character(time_TxIdx, format="%Y"))
  
  ## Get variables:
  var_vec <- names(nc_file_TxIdx$var)
  covar_vec <- sort(var_vec[c(grep("ABS",var_vec),
                              grep("ANO",var_vec),
                              grep("STD",var_vec))])
  
  ## Get coordinates and get them to be from -180 to 180°Lon
  var_lat  <- nc_file_TxIdx$dim$lat_2
  var_lon  <- nc_file_TxIdx$dim$lon_2
  coord_vec <- expand.grid(lon=var_lon$vals,lat=var_lat$vals)
  
  ## Check where lon-information starts (-180° or 0°)
  ind_cor <- with(coord_vec, order(lat, lon))
  if(!is.ordered(ind_cor)) coord_vec <- coord_vec[ind_cor,]
  
  ## Add fixed variables:
  coord_vec$lon_orig  <- coord_vec$lon
  coord_vec$coord     <- paste0("coord_",coord_vec$lon,"_",coord_vec$lat)
  coord_vec$Orography <- as.numeric(ncvar_get(nc_file_TxIdx, "orog"))
  coord_vec$Landfrac  <- as.numeric(ncvar_get(nc_file_TxIdx, "sftlf"))
  coord_vec$GP_Area   <- as.numeric(ncvar_get(nc_file_TxIdx, "areacella"))
  coord_vec$Landfrac[coord_vec$Landfrac<  0] <- 0
  coord_vec$Landfrac[coord_vec$Landfrac>100] <- 100
  
  ## Get plotting longitudes (for locs not close to 180°E):
  range_lon <- diff(range(coord_vec$lon))/2
  mean_lon  <- mean(unique(coord_vec$lon))
  if(!((180-range_lon)<mean_lon & (180+range_lon)>mean_lon))
    coord_vec$lon[coord_vec$lon>180] <- coord_vec$lon[coord_vec$lon>180]-360
  
  ## Read in covariates:
  res_ls <- list(coord=coord_vec)
  for(covar in covar_vec) {
    if(length(dim((ncvar_get(nc_file_TxIdx, covar))))!=3) next
    
    ## Create basis data.frame:
    res_df <- data.frame(
      model   =factor(model_sname,levels=model_lvl, ordered=T),
      ens     =ens,
      exp     =factor(exp,levels=exp_lvl, ordered=T),
      location=factor(loc_sname,levels=loc_lvl, ordered=T),
      max_seas=factor(max_seas,levels=maxs_lvl, ordered=T),
      year    =year_TxIdx,
      row.names = NULL
    )
    
    ## Get variable into the right dimension:
    var_mat <- (ncvar_get(nc_file_TxIdx, covar))
    dim(var_mat) <- c(prod(dim(var_mat)[1:2]),dim(var_mat)[3])
    dimnames(var_mat)[[2]] <- year_TxIdx
    var_mat <- t(var_mat)
    var_df <- data.frame(var_mat)
    
    ## Append to basis data.frame
    names(var_df) <- coord_vec$coord
    res_df <- cbind(res_df, var_df)
    
    if(max_seas=="DJF") {
      res_df <- res_df[-c(1,nrow(res_df)),]
    }
    
    res_ls[[covar]] <- res_df
  }
  return(res_ls)
}

read_var <- function(base_path, location, model, max_index, which_var="1d") {
  base_exp <- ifelse(model=="ERA5","reanalysis","piControl")
  model_sname <- model
  model_lname <- model_ls[[model]]$mod_lname
  model_set   <- model_ls[[model]]$mod_setting
  model_exp   <- model_ls[[model]]$EXP
  model_ens   <- model_ls[[model]]$ENS
  integration_time <- substr(max_index,3,3)
  
  exp_ens_df <- rbind(data.frame(ens=1,exp=base_exp),
                      expand.grid(ens=model_ens, exp=model_exp[-1]))
  
  file_names <- c(paste0("VARIABLES_",model_lname,"_",base_exp,"_r1i",model_set,"_g025.RUNMEAN",integration_time,".",location,".",max_index,"IDX.nc"),
                  paste0("VARIABLES_",model_lname,"_",exp_ens_df$exp[-1],paste0("_r",exp_ens_df$ens[-1],"i"),
                         model_set,"_g025.RUNMEAN",integration_time,".",location,".",max_index,"IDX.nc"))
  
  exp_ens_df  <- cbind(exp_ens_df, file_names=file.path(base_path,location,"merge",file_names))
  if(length(model_ls[[model]]$EXP)==1) exp_ens_df <- exp_ens_df[1,]

  if(which_var=="1d") {
    data_1d_ls     <- apply(as.matrix(exp_ens_df), 1, FUN=read_1d_nc,
                         model_sname=model_sname,
                         loc_sname=location,
                         max_seas=location_df$max_seas[location_df$loc_sname==location],
                         max_index=max_index)
    data_1d_df <- do.call(rbind,data_1d_ls)
    return(data_1d_df)
  } else if(which_var=="3d") {
    data_3d_ls     <- apply(as.matrix(exp_ens_df), 1, FUN=read_3d_nc,
                         model_sname=model_sname,
                         loc_sname=location,
                         max_seas=location_df$max_seas[location_df$loc_sname==location],
                         max_index=max_index)
    data_3d_ls_new <- list(coord=data_3d_ls[[1]]$coord)
    
    for(var_name in names(data_3d_ls[[1]])[-1]) {
      list_var_name <- list()
      for(exp_ens_comb in 1:length(data_3d_ls)) {
        list_var_name[[exp_ens_comb]] <- data_3d_ls[[exp_ens_comb]][[var_name]]
        if(!all.equal(data_3d_ls[[exp_ens_comb]]$coord,data_3d_ls_new$coord))
          stop(paste0("Coordinates are not the same for model ",model," at loc ",location))
      }
      data_3d_ls_new[[var_name]] <- do.call(rbind,list_var_name)
    }
    return(data_3d_ls_new)
  }
}


## Get multi-year block maxima:
get_nyr_BM <- function(res_df_1d, BM_length=5) {
  ## Add index variable:
  res_df_1d[,paste0("BM",BM_length)] <- FALSE
  res_df_1d$ind <- 1:nrow(res_df_1d)
  
  ## And ensemble rank variable (since not all ens-numberings are continous):
  res_df_1d <- res_df_1d %>% group_by(model) %>% summarise(ens_unique=unique(ens)) %>%
    mutate(ens_rank=rank(ens_unique)) %>% rename(ens=ens_unique) %>%
    merge(res_df_1d, ., by=c("model","ens"))
  
  ind_nyr_BM <- res_df_1d %>% filter(exp %in% c("piControl","reanalysis")) %>%
    group_by(location, model) %>% 
    mutate(year_round=factor(floor(year/BM_length))) %>% #summarise(nyr     =n_distinct(year_round))
    ungroup() %>% group_by(location, model, year_round) %>% 
    filter(Tx7d==max(Tx7d),.preserve = T) %>% ungroup() %>%
    select(ind) %>% .$ind
  # for(i_ens in seq(1,floor(max(res_df_1d$ens)/BM_length),BM_length)) {
  for(i_ens in seq(1,floor(max(res_df_1d$ens_rank)),BM_length)) {
    ens_i_upper <- BM_length-1+i_ens
    if(ens_i_upper>max(res_df_1d$ens_rank)) next
    BM_5yr_trans_ind <- res_df_1d %>%
      filter(!(exp %in% c("piControl","reanalysis")),
             ens_rank %in% i_ens:ens_i_upper) %>%
      group_by(location, model, exp, year) %>%
      filter(Tx7d==max(Tx7d),.preserve = T) %>%
      ungroup() %>% select(ind) %>% .$ind
    ind_nyr_BM <- append(ind_nyr_BM, BM_5yr_trans_ind)
  }
  
  if(length(ind_nyr_BM)!=length(unique(ind_nyr_BM)))
    stop("5yr BM indices are not unique")
  res_df_1d[ind_nyr_BM,paste0("BM",BM_length)] <- T
  return(res_df_1d[,paste0("BM",BM_length)])
}
