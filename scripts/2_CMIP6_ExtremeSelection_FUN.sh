#!/bin/bash

###### HEADER - 2_CMIP6_ExtremeSelection_FUN.sh ##############################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    20.09.2021
# Purpose: With this script, extremes and the respective covariates are 
#          selected and written to a final NetCDF file - FUNCTIONS.
##############################################################################

#### DEFINE FUNCTIONS ########################################################

## Get location specific TxNd time series (data) and indices:
get_extremes_data_ind () {
  ## Should running mean be calculated? Not in case of mrsol variable:
  if [ "$max_var_in" == "mrsol" ]; then
    nameext=""
    rmlen=1
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
    rmlen=$RUNMEAN_LEN
  fi

  ## Get input and output filenames:
  filename_in=${max_var_in}_${max_tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.nc
  file_in=$filepath_maxvar_in$filename_in

  mkdir -pv ${wd_path}/${LOC}/${max_index_name}/
  filepath_out=${wd_path}/${LOC}/${max_index_name}/
  filename_out=${max_index_name}_${max_tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.nc
  file_out=$filepath_out$filename_out
  
  filename_idx_out=${max_index_name}_${max_tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025_idx
  file_idx_out=$filepath_out$filename_idx_out
  
  filename_maxvar_out=${max_var_in}_${max_tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.${LOC}.nc
  file_maxvar_out=$filepath_out$filename_maxvar_out

  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]
  then
    if [ "$verbose" = "TRUE" ]; then echo "      Getting annual maxima of file: $filename_in"; fi

    ## Check whether the season for maxima is JJA or DJF
    if [[ "$loc_maxseas" == "DJF" ]]
    then
      ## Shift winter maxima by 180 days and get the respective maxima:
      cdo -s shifttime,+180days, -fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN, $file_in $file_maxvar_out
      cdo -s shifttime,-180days, -yearmax $file_maxvar_out $file_out
      
      ## Get index of annual maxima (+180 days), first set missing value to -1:
      cdo -s yearmaxidx, -setmissval,-1 $file_maxvar_out $file_idx_out.pl180.nc
    fi 
    
    if [[ "$loc_maxseas" == "JJA" ]]
    then
      ## Get the respective maxima:
      cdo -s    fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN, $file_in $file_maxvar_out
      cdo -s    yearmax $file_maxvar_out $file_out
      
      ## Get index of annual maxima, first set missing value to -1:
      cdo -s yearmaxidx, -setmissval,-1 $file_maxvar_out $file_idx_out.nc
    fi
    
  fi
  
}

## Get boundaries of regional subset around locations of interest:
get_regional_boundaries () {
  ## Get borders of region around location:
  reg_lonE=$(bc <<< "scale=2;$loc_lonW/2+$loc_lonE/2+$( grep ^Extend\| $model_info_file | cut -d"|" -f2 )")
  reg_lonW=$(bc <<< "scale=2;$loc_lonW/2+$loc_lonE/2+$( grep ^Extend\| $model_info_file | cut -d"|" -f3 )")
  reg_latS=$(bc <<< "scale=2;$loc_latS/2+$loc_latN/2+$( grep ^Extend\| $model_info_file | cut -d"|" -f4 )")
  reg_latN=$(bc <<< "scale=2;$loc_latS/2+$loc_latN/2+$( grep ^Extend\| $model_info_file | cut -d"|" -f5 )")
}

## Get fields of tas and zg500 (variables at daily temporal resolution)
## at indices of annual maxima:
get_daily_variable_fields () {
  ## Get path to file with yearmax indices:
  filepath_idx=${wd_path}/${LOC}/${max_index_name}/
  filename_idx=${max_index_name}_${max_tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025_idx
  file_idx=$filepath_idx$filename_idx
  
  for VAR in $VARIABLES_DAY
  do 
    ## Get name extension:
    if [ "$VAR" == "mrsol" ]; then
      nameext=""
    else
      nameext=.RUNMEAN${RUNMEAN_LEN}
    fi

    ## Define filepath to be read:
    filepath_in=${wd_path}/../${VAR}/day/g025/
    filename_in=${VAR}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.nc
    file_in=$filepath_in$filename_in

    ## Define file path for the file with the absolute values:
    mkdir -pv ${wd_path}/${LOC}/${VAR}/
    filepath_out=${wd_path}/${LOC}/${VAR}/
    filename_out_abs=${VAR}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.${LOC}reg.${max_index_name}IDX.ABS.nc
    file_out_abs=$filepath_out$filename_out_abs
    
    
    ## Filename of the final (merged) file:
    filename_out_merge=${VAR}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.${LOC}reg.${max_index_name}IDX.nc
    file_out_merge=$filepath_out$filename_out_merge

    if [ "$verbose" = "TRUE" ]; then echo "      - Working on variable $VAR in file path: $filepath_out"; fi
    
    ## Check whether output file already exists (and if not overwrite, return):
    if [[ ( ! -f $file_out_abs || "$overwrite" == "TRUE" ) && ( ! -f $file_out_merge || "$overwrite" == "TRUE" ) ]]
    then
      if [ "$verbose" = "TRUE" ]; then echo "         Writing $max_index_name regional subset ($LOC) to file: $filename_out_abs"; fi
  
      ## Check whether the season for maxima is JJA or DJF
      if [[ "$loc_maxseas" == "DJF" ]]
      then
        ## Shift winter maxima by 180 days and get the respective maxima:
        cdo -s selvar,$max_var_in -enlarge,$file_in $file_idx.pl180.nc $file_idx.pl180.FLD.nc
        cdo -s shifttime,-180days, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN, -selyearidx $file_idx.pl180.FLD.nc -shifttime,+180days -select,name=$VAR $file_in $file_out_abs.TEMP
        # cdo -s enlarge,$file_in $file_idx.pl180.nc $filepath_out/test_idx.nc
        # cdo shifttime,+180days $file_in $filepath_out/test1.nc
        # cdo selyearidx $filepath_out/test_idx.nc $filepath_out/test1.nc $filepath_out/test2.nc
        # cdo sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN, $filepath_out/test2.nc $filepath_out/test3.nc
        # cdo shifttime,-180days, $filepath_out/test3.nc $filepath_out/test4.nc
        rm -rf $file_idx.pl180.FLD.nc
      fi 
      
      if [[ "$loc_maxseas" == "JJA" ]]
      then
        ## Get the respective maxima:
        cdo -s selvar,$max_var_in -enlarge,$file_in $file_idx.nc $file_idx.FLD.nc
        cdo -s sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN, -selyearidx $file_idx.FLD.nc -select,name=$VAR $file_in $file_out_abs.TEMP
        rm -rf $file_idx.FLD.nc
      fi
      
      cdo -s setname,${VAR}_ABS, -selvar,$VAR $file_out_abs.TEMP $file_out_abs
      rm -rf $file_out_abs.TEMP
    fi
  
    ## Define file path for the file with the anomalies/standard deviations:
    filename_out_ano=${VAR}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.${LOC}reg.${max_index_name}IDX.ANO.nc
    filename_out_std=${VAR}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.${LOC}reg.${max_index_name}IDX.STD.nc
    file_out_ano=$filepath_out$filename_out_ano
    file_out_std=$filepath_out$filename_out_std
  
    
    ## Check whether output file already exists (and if not overwrite, return):
    if [[ ( ! -f $file_out_ano || ! -f $file_out_std || "$overwrite" == "TRUE" ) && ( ! -f $file_out_merge || "$overwrite" == "TRUE" ) ]]
    then
      if [ "$verbose" = "TRUE" ]; then echo "         Calculate anomalies/standard deviations of file: $filename_out_abs"; fi
  
      ## Path of seasonal trend file:
      filepath_seas_in=${wd_path}/../${VAR}/day/g025/_SEASMEAN/_TREND/
      if [[ "$EXP" == "piControl" ]]
      then
        filename_seas_in=${VAR}_day_${mod_lname}_${EXP}_r1i${mod_setting}_g025${nameext}.${loc_maxseas}MEAN.TREND.nc
      else
        filename_seas_in=${VAR}_day_${mod_lname}_${EXP}_ENSMEAN_g025${nameext}.${loc_maxseas}MEAN.TREND.nc
      fi
      file_seas_in=$filepath_seas_in$filename_seas_in
      
      ## Path of daily variability file:
      filepath_dailyvar_in=${wd_path}/../${VAR}/day/g025/_DAILYVAR/
      filename_dailyvar_in=${VAR}_day_${mod_lname}_piControl_r1i${mod_setting}_g025${nameext}.${loc_maxseas}DAILYVAR.nc
      file_dailyvar_in=$filepath_dailyvar_in$filename_dailyvar_in
      
      ## Get years to select from file with smoothed seasonal means:
      selyears_vec=$( echo $( cdo -s showyear $file_out_abs ) | sed -r 's/ /,/g' )
      
      ## Calculate anomalies against seasonal mean trend (out of some reason, I have to use this version of CDO for this command,
      ## otherwise it selects to few longitude(s) extends..):
      /usr/local/bin/cdo -s selyear,$selyears_vec  -selvar,${VAR}_trend, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $file_seas_in $file_out_ano.SUB
      cdo -s setname,${VAR}_ANO, -selvar,${VAR}_ABS, -sub, $file_out_abs $file_out_ano.SUB  $file_out_ano
      # cdo -s setname,${VAR}_ANO, -selvar,${VAR}_ABS, -sub, $file_out_abs -selyear,$selyears_vec  -selvar,${VAR}_trend, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $file_seas_in     $file_out_ano
      
      cdo -s selvar,${VAR}, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $file_dailyvar_in $file_out_std.DIV
      cdo -s setname,${VAR}_STD, -selvar,${VAR}_ANO, -div, $file_out_ano $file_out_std.DIV $file_out_std
      rm -rf $file_out_std.DIV $file_out_ano.SUB
      # cdo -s setname,${VAR}_STD, -selvar,${VAR}_ANO, -div, $file_out_ano                         -selvar,${VAR},       -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $file_dailyvar_in $file_out_std
    fi
    
    ## Merge into one file:
    if [[ ! -f $file_out_merge || "$overwrite" == "TRUE" ]]
    then
      if [ "$verbose" = "TRUE" ]; then echo "         Merge into file: $filename_out_merge"; fi
      if [[ "$overwrite" == "TRUE" ]] ; then rm -rf $file_out_merge ; fi
      cdo -s merge $file_out_abs $file_out_ano $file_out_std $file_out_merge
      rm -rf  $file_out_abs $file_out_ano $file_out_std
    fi
  done
  
}


## Get fields of mrsol (variable at monthly temporal resolution)
## at indices of annual maxima (weighted mean of concurrent and previous
## month):
get_monthly_variable_fields () {
  ## Get path to file with yearmax indices (and temperature):
  filepath_idx=${wd_path}/${LOC}/${max_index_name}/
  filename_idx=${max_index_name}_${max_tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025_idx
  file_idx=$filepath_idx$filename_idx
  
  filename_Tx=${max_index_name}_${max_tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.nc
  file_Tx=$filepath_idx$filename_Tx
  
  for VAR in $VARIABLES_MON
  do 
    ## Get name extension:
    if [ "$VAR" == "mrsol" ]; then
      nameext=""
    else
      nameext=.RUNMEAN${RUNMEAN_LEN}
    fi
    
    ## Define filepath of monthly variable to be read:
    filepath_in=${wd_path}/../${VAR}/mon/g025/
    filename_in=${VAR}_mon_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.nc
    file_in=$filepath_in$filename_in
    
    ## Define output file path for the file with the absolute values:
    mkdir -pv ${wd_path}/${LOC}/${VAR}/
    filepath_out=${wd_path}/${LOC}/${VAR}/
    filename_out_abs=${VAR}_mon_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.${LOC}loc.${max_index_name}IDX.ABS.nc
    file_out_abs=$filepath_out$filename_out_abs
    if [ "$verbose" = "TRUE" ]; then echo "      - Working on variable $VAR in file path: $filepath_out"; fi
    
    ## Get local series of mrsol:
    filename_in_loc=${VAR}_mon_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.${LOC}loc.nc
    file_in_loc=$filepath_out$filename_in_loc
    ## Check whether output file already exists (and if not overwrite, return):
    if [[ ! -f $file_out_abs || "$overwrite" == "TRUE" ]]
    then
      if [ "$verbose" = "TRUE" ]; then echo "         Writing $max_index_name regional subset ($LOC, weighted mean) to file: $filename_out_abs"; fi
      cdo -s fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN $file_in $file_in_loc
    
      if [[ "$loc_maxseas" == "DJF" ]]
      then
        # echo $file_idx.pl180.nc $file_in_loc $file_out_abs $VAR $file_Tx
        /usr/local/R-4.0.3-openblas/bin/Rscript $mon_maxidx_file "$file_idx.pl180.nc" "$file_in_loc" "$file_out_abs.TEMP" "$VAR" "$file_Tx"
        # exit 1
      fi 
      
      if [[ "$loc_maxseas" == "JJA" ]]
      then
        /usr/local/R-4.0.3-openblas/bin/Rscript $mon_maxidx_file "$file_idx.nc" "$file_in_loc" "$file_out_abs.TEMP" "$VAR" "$file_Tx"
      fi
      
      cdo -s setname,${VAR}_ABS, -selvar,$VAR $file_out_abs.TEMP $file_out_abs
      rm -rf $file_in_loc $file_out_abs.TEMP
    fi
  
    ## Define file path for the file with the anomalies/standard deviations:
    filename_out_ano=${VAR}_mon_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.${LOC}loc.${max_index_name}IDX.ANO.nc
    filename_out_std=${VAR}_mon_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.${LOC}loc.${max_index_name}IDX.STD.nc
    file_out_ano=$filepath_out$filename_out_ano
    file_out_std=$filepath_out$filename_out_std
  
    
    ## Check whether output file already exists (and if not overwrite, return):
    if [[ ! -f $file_out_ano || ! -f $file_out_std || "$overwrite" == "TRUE" ]]
    then
      if [ "$verbose" = "TRUE" ]; then echo "         Calculate anomalies/standard deviations of file: $filename_out_abs"; fi
  
      ## Path of seasonal trend file:
      filepath_seas_in=${wd_path}/../${VAR}/mon/g025/_SEASMEAN/_TREND/
      if [[ "$EXP" == "piControl" ]]
      then
        filename_seas_in=${VAR}_mon_${mod_lname}_${EXP}_r1i${mod_setting}_g025${nameext}.${loc_maxseas}MEAN.TREND.nc
      else
        filename_seas_in=${VAR}_mon_${mod_lname}_${EXP}_ENSMEAN_g025${nameext}.${loc_maxseas}MEAN.TREND.nc
      fi
      file_seas_in=$filepath_seas_in$filename_seas_in
      
      ## Path of monthly variability file:
      filepath_dailyvar_in=${wd_path}/../${VAR}/mon/g025/_DAILYVAR/
      filename_dailyvar_in=${VAR}_mon_${mod_lname}_piControl_r1i${mod_setting}_g025.${loc_maxseas}DAILYVAR.nc
      file_dailyvar_in=$filepath_dailyvar_in$filename_dailyvar_in
      
      ## Get years to select from file with smoothed seasonal means:
      selyears_vec=$( echo $( cdo -s showyear $file_out_abs ) | sed -r 's/ /,/g' )
      
      ## Calculate anomalies against seasonal mean trend:
      cdo -s selvar,${VAR}_trend, -fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN $file_seas_in $file_out_ano.temp
      cdo -s setname,${VAR}_ANO, -selvar,${VAR}_ABS, -sub, $file_out_abs $file_out_ano.temp $file_out_ano
      rm -rf $file_out_ano.temp
      
      # cdo -s setname,${VAR}_ANO, -selvar,${VAR}_ABS, -sub, $file_out_abs -selyear,$selyears_vec -selvar,${VAR}_trend, -fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN $file_seas_in     $file_out_ano
      cdo -s setname,${VAR}_STD, -selvar,${VAR}_ANO, -div, $file_out_ano                        -selvar,${VAR},       -fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN $file_dailyvar_in $file_out_std
    fi
    
    ## Merge into one file:
    filename_out_merge=${VAR}_mon_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.${LOC}loc.${max_index_name}IDX.nc
    file_out_merge=$filepath_out$filename_out_merge
    if [[ ! -f $file_out_merge || "$overwrite" == "TRUE" ]]
    then
      if [ "$verbose" = "TRUE" ]; then echo "         Merge into file: $filename_out_merge"; fi
      if [[ "$overwrite" == "TRUE" ]] ; then rm -rf $file_out_merge ; fi
      cdo -s merge $file_out_abs $file_out_ano $file_out_std $file_out_merge
      rm -rf  $file_out_abs $file_out_ano $file_out_std
    fi
  done
}


## Get fixed fields (orography/land-fraction/grid-size):
get_fx_variable_fields () {
  for VAR in $VARIABLES_FIX
  do 
    ## Define filepath of fixed variable to be read:
    filepath_in=${wd_path}/../${VAR}/fx/g025/
    filename_in=${VAR}_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.nc
    file_in=$filepath_in$filename_in
    
    ## Define output file path for the file with the absolute values:
    mkdir -pv ${wd_path}/${LOC}/${VAR}/
    filepath_out=${wd_path}/${LOC}/${VAR}/
    filename_out=${VAR}_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.${LOC}loc.nc
    file_out=$filepath_out$filename_out
    
    ## Get subset of fixed variables:
    if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]; then
      if [ "$verbose" = "TRUE" ]; then echo "      - Working on variable $VAR in file path: $filepath_out"; fi
      cdo -s sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $file_in $file_out
    fi
  done
}

## Merge data into one NetCDF file per location / forcing:
merge_data () {
  ## List of files to be merged:
  files_list=""
  
  ## Get files in:
  filepath_in=${wd_path}/${LOC}/
  
  ## Use model configuratin or ENSEMAN (for historical and SSPs):
  if [ "$EXP" == "piControl" ]; then
    conf_ensmean=r${ens_i}i${mod_setting}
  else
    conf_ensmean=ENSMEAN
  fi
  
  ## Extreme value vector:
  filepath_max_in=${filepath_in}/${max_index_name}/
  filename_maxval_in=${max_index_name}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.nc
  file_maxval_in=$filepath_max_in$filename_maxval_in
  files_list="$files_list -setname,$max_index_name -selvar,$max_var_in $file_maxval_in"

  ## Get years to select from file with smoothed seasonal means:
  selyears_vec=$( echo $( cdo -s showyear $file_maxval_in ) | sed -r 's/ /,/g' )
  
  ## Extreme value index vector:
  if [[ "$loc_maxseas" == "DJF" ]]
  then
    filename_maxidx_in=${max_index_name}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025_idx.pl180.nc
  fi
  if [[ "$loc_maxseas" == "JJA" ]]
  then
    filename_maxidx_in=${max_index_name}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025_idx.nc
  fi
  file_maxidx_in=$filepath_max_in$filename_maxidx_in
  files_list="$files_list -setname,${max_index_name}_IDX -selvar,$max_var_in $file_maxidx_in"
  
  ## Smoothed global mean surface temperature:
  filepath_tasannmean_in=${wd_path}/../tas/day/g025/_FLDYRMEAN/
  filename_tasannmean_in=tas_day_${mod_lname}_${EXP}_${conf_ensmean}_g025.RUNMEAN${RUNMEAN_LEN}.FLDYRMEAN.nc
  file_tasannmean_in=$filepath_tasannmean_in$filename_tasannmean_in
  files_list="$files_list -setname,gmst -selvar,tas, -selyear,$selyears_vec, $file_tasannmean_in -setname,gmst_smooth, -selvar,tas_smooth, -selyear,$selyears_vec, $file_tasannmean_in"
  
  ## Predictor variables with daily resolution:
  for VAR in $VARIABLES_DAY; do
    ## Get name extension:
    if [ "$VAR" == "mrsol" ]; then
      nameext=""
    else
      nameext=.RUNMEAN${RUNMEAN_LEN}
    fi
    filename_VAR_day=${VAR}/${VAR}_day_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.${LOC}reg.${max_index_name}IDX.nc
    files_list="$files_list $filepath_in$filename_VAR_day"
    files_list="$files_list -setname,${VAR}_ABS_LOC, -selvar,${VAR}_ABS, -fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN $filepath_in$filename_VAR_day"
    files_list="$files_list -setname,${VAR}_ANO_LOC, -selvar,${VAR}_ANO, -fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN $filepath_in$filename_VAR_day"
    files_list="$files_list -setname,${VAR}_STD_LOC, -selvar,${VAR}_STD, -fldmean, -sellonlatbox,$loc_lonE,$loc_lonW,$loc_latS,$loc_latN $filepath_in$filename_VAR_day"
    
    filename_VARtrend_day=${VAR}/day/g025/_SEASMEAN/_TREND/${VAR}_day_${mod_lname}_${EXP}_${conf_ensmean}_g025${nameext}.${loc_maxseas}MEAN.TREND.nc
    files_list="$files_list -selvar,${VAR}_beta0_forc, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $wd_path/../$filename_VARtrend_day"
    files_list="$files_list -selvar,${VAR}_beta1_forc, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $wd_path/../$filename_VARtrend_day"
    files_list="$files_list -selvar,${VAR}_beta1_forc_stderr, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $wd_path/../$filename_VARtrend_day"
  done
  
  ## Predictor variables with monthly resolution:
  for VAR in $VARIABLES_MON; do
    filename_VAR_mon=${VAR}/${VAR}_mon_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025.${LOC}loc.${max_index_name}IDX.nc
    files_list="$files_list $filepath_in$filename_VAR_mon"
    
    filename_VARtrend_day=${VAR}/mon/g025/_SEASMEAN/_TREND/${VAR}_mon_${mod_lname}_${EXP}_${conf_ensmean}_g025.${loc_maxseas}MEAN.TREND.nc
    files_list="$files_list -selvar,${VAR}_beta0_forc, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $wd_path/../$filename_VARtrend_day"
    files_list="$files_list -selvar,${VAR}_beta1_forc, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $wd_path/../$filename_VARtrend_day"
    files_list="$files_list -selvar,${VAR}_beta1_forc_stderr, -sellonlatbox,$reg_lonE,$reg_lonW,$reg_latS,$reg_latN $wd_path/../$filename_VARtrend_day"
  done
  
  
  ## Fixed variables:
  for VAR in $VARIABLES_FIX; do
    filename_VAR_fix=${VAR}/${VAR}_fx_${mod_lname}_piControl_r1i${mod_setting}_g025.${LOC}loc.nc
    files_list="$files_list $filepath_in$filename_VAR_fix"
  done
  
  ## Merged file out:
  filepath_out=${wd_path}/${LOC}/merge/
  filename_out=VARIABLES_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.${LOC}.${max_index_name}IDX.nc
  file_merge_out=$filepath_out$filename_out
  mkdir -pv $filepath_out
  
  ## Merge files:
  if [[ ! -f $file_merge_out || "$overwrite" == "TRUE" ]]
  then
    if [ "$verbose" = "TRUE" ]; then echo "        Merging variables to file: $filename_out"; fi
    if [[ "$overwrite" == "TRUE" ]] ; then rm -rf $file_merge_out ; fi
    cdo -s merge $files_list $file_merge_out
  fi
}