#!/bin/bash

###### HEADER - 2_CMIP6_ExtremeSelection.sh #################################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    13.08.2021
# Purpose: With this script, extremes and the respective covariates are 
#          selected and written to a final NetCDF file.
##############################################################################

## Overwrite existing files? / Print additional information?
overwrite=FALSE
verbose=TRUE

## Define path to config file:
# model_info_file=/home/jzeder/Projects/nonstationary-extremes/scripts/1_CMIP6_Preprocessing_Rx5d.conf
# model_info_file=/home/jzeder/Projects/nonstationary-extremes/scripts/1_CMIP6_Preprocessing.conf
# model_info_file=/home/jzeder/Projects/nonstationary-extremes/scripts/1_CESM12_Preprocessing.conf
model_info_file=/home/jzeder/Projects/nonstationary-extremes/scripts/1_Reanalysis_Preprocessing.conf

## Define working directory path:
wd_path=$( grep ^"wd_path"\| $model_info_file | cut -d"|" -f2)
wd_path=${wd_path}/Locs/
mkdir -pv $wd_path
cd $wd_path

## Additional scripts used:
bashscript_functions1=~/Projects/nonstationary-extremes/scripts/1_CMIP6_Preprocessing_FUN.sh
bashscript_functions2=~/Projects/nonstationary-extremes/scripts/2_CMIP6_ExtremeSelection_FUN.sh
mon_maxidx_file=~/Projects/nonstationary-extremes/scripts/2_mrsol_mon_maxidx.R

#### DEFINE FUNCTIONS ########################################################
module load cdo/1.9.9

## Copy grid files:
source $bashscript_functions1
source $bashscript_functions2

#### MAIN BODY ###############################################################

## MGet user input and ask for "GO":
if [ $# -eq 0 ]
  then
  MODELS=$( grep ^"Models"\| $model_info_file | cut -d"|" -f2)
  LOCS=$( grep ^"Locations"\| $model_info_file | cut -d"|" -f2)
else
  MODELS=$1
  LOCS=$2
  if [[ "$LOCS" == "All" || "$LOCS" == "ALL" || "$LOCS" == "all" ]]; then
    LOCS=$( grep ^"Locations"\| $model_info_file | cut -d"|" -f2)
  fi
fi
get_variable_tres_list
echo ""
echo ""
echo "      === Obtaining maxima for models $MODELS and locations $LOCS ==="
echo "          Variables: $VARIABLES_DAY (day) / $VARIABLES_MON (mon)"
echo ""
echo ""
sleep 3s

## Define variables to be used to retrieve maxima and the name of the index:
RUNMEAN_LEN=$( grep ^"runmean_len"\| $model_info_file | cut -d"|" -f2)
max_var_in=$( grep ^"max_var_in"\| $model_info_file | cut -d"|" -f2)
max_tres_in=$( grep ^"max_tres_in"\| $model_info_file | cut -d"|" -f2)
max_index_name=$( grep ^"max_index_name"\| $model_info_file | cut -d"|" -f2)

## Path to variable providing the maxima (probably defined as 'tas' above):
filepath_maxvar_in=${wd_path}/../${max_var_in}/${max_tres_in}/g025/

## Loop over existing CMIP6ng datasets:
for LOC in $LOCS
do
  if [ "$verbose" = "TRUE" ]; then echo "Working on location: $LOC"; fi

  ## Get location-specific information:
  loc_lname=$( grep ^$LOC\| $model_info_file | cut -d"|" -f2 )
  loc_lonE=$( grep ^$LOC\| $model_info_file | cut -d"|" -f3 )
  loc_lonW=$( grep ^$LOC\| $model_info_file | cut -d"|" -f4 )
  loc_latS=$( grep ^$LOC\| $model_info_file | cut -d"|" -f5 )
  loc_latN=$( grep ^$LOC\| $model_info_file | cut -d"|" -f6 )
  loc_maxseas=$( grep ^$LOC\| $model_info_file | cut -d"|" -f7 )
  
  ## Set location-specific regional boundaries:
  get_regional_boundaries

  ## Loop over variables:
  for MODEL in $MODELS
  do
    if [ "$verbose" = "TRUE" ]; then echo "  - Working on model: $MODEL"; fi

    ## Get temporal/spatial resolution for different variables:
    mod_lname=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f2 )
    mod_setting=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f3 )
    mod_exp_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f4 )
    # mod_ensind_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f5 )
    mod_ensind_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f5 )
    mod_ensind_list=$( eval echo $mod_ensind_list )

    ## Loop over experiments:
    for EXP in $mod_exp_list
    do
      if [ "$verbose" = "TRUE" ]; then echo "    - Working on experiment: $EXP"; fi
      if [ "$EXP" = "piControl" ]; then
        ens_i=1
        get_extremes_data_ind
        get_daily_variable_fields
        get_monthly_variable_fields
        get_fx_variable_fields
        merge_data
      else
        for ens_i in $mod_ensind_list
        do
          if [ "$verbose" = "TRUE" ]; then echo "    - Working on ensemble: $ens_i"; fi
          get_extremes_data_ind
          get_daily_variable_fields
          get_monthly_variable_fields
          if [ "$EXP" = "reanalysis" ]; then get_fx_variable_fields; fi
          merge_data
          if [ "$verbose" = "TRUE" ]; then echo ""; fi
        done
      fi
      
    done

  done

done


## Send notification email:
duration=$SECONDS
mail -s "Getting $max_index_name and covariates for $MODELS and $LOCS ($(($duration / 60))min $(($duration % 60))s)" joel.zeder@env.ethz.ch < /dev/null
exit 0










## Mini-script to rename files:
# for LOC in WEU PNW WRU CNA EAS SWA SAF
# do
#   path=/net/xenon/climphys/jzeder/Projects/nonstationary-extremes/data/CMIP6/Locs/${LOC}/Tx7d/
#   file_list=$( ls $path/*_LOC.nc )
#   for FILE in $file_list
#   do
#     file_path_in=$FILE
#     file_path_out=${file_path_in::-7}.${LOC}.nc
#     mv $file_path_in $file_path_out
#     # echo $file_path_in
#     # echo $file_path_out
#   done
# done


