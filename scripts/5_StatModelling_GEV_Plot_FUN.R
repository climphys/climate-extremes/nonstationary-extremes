#!/bin/Rscript

###### HEADER - 5_StatModelling_GEV_FUN.R ####################################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    19.11.2021
# Purpose: Fit statistical model (ridge for warm start and regularised GEV)
#          (FUNCTIONS related to regularised GEV plotting).
##############################################################################

## Plot evaluation scores:
plot_evaluation_scores <- function(score_df, CI_PID_df, train_test, model_train,
                                   opt_lambda) {
  
  ## Get title:
  title <- bquote(.(model_train)~"GEV fit"~.(train_test)~"-"~lambda[opt]==10^.(log10(opt_lambda)))
  
  ## Plot evaluation plots:
  plot_GEVmean_obs <- ggplot(score_df, aes(x=mu, y=y)) +
    ggtitle(title) +
    geom_point(alpha=0.1) +
    geom_density2d(col="orange",alpha=0.7) +
    scale_x_continuous(name=bquote("Predicted"~widehat(mu)),
                       labels=function(x) paste0(x,"°C")) +
    scale_y_continuous(name=bquote("Tx7d (observed)"),
                       labels=function(x) paste0(x,"°C")) +
    geom_abline(slope=1, col="darkred")
  
  plot_PIT <- ggplot(score_df, aes(x=quant)) +
    scale_x_continuous(name=bquote("PIT"~widehat(F)[Y](y)),
                       labels = scales::percent_format(accuracy = 1),
                       breaks=seq(0,1,0.2),limits = c(-0.01,1.01),expand = c(0,0)) +
    scale_y_continuous(name="Density") +
    geom_hline(yintercept = 0, color="grey20") +
    geom_line(data=CI_PID_df, aes(x=x, y=X2.5.), col="darkred",alpha=0.5) +
    geom_line(data=CI_PID_df, aes(x=x, y=X97.5.), col="darkred",alpha=0.5) +
    geom_histogram(alpha=0.4, color="grey20", aes(y=..density..), breaks=seq(0,1,0.1)) +
    geom_segment(x=0,xend=1,y=1,yend=1,col="darkred") +
    geom_point(data=data.frame(x=c(0,1),y=c(1,1)),aes(x=x,y=y),col="darkred")
  res_plot <- ggarrange(plot_GEVmean_obs,plot_PIT,align = "v",ncol = 1)
  
  ## Save plot to disk:
  dir.create(plotpath_gev_eval <- file.path(plotpath_gev,"1_EvalGEV"),
             showWarnings=F, recursive=T)
  save_plot(res_plot,
            file.path(plotpath_gev_eval,
                      paste("1_EvalGEV",train_test,loc_sname,model_train,sep="_")),
            width=5,height=8)
}

## Plot comparison of l2-norms (scatterplot):
l2norm_comparison <- function(gev_res_all_covar_eval, model_train) {
  ## Get title:
  title <- bquote(.(model_train)~italic(l)^2~"comparison")
  scatterplot <- gev_res_all_covar_eval %>% ggplot(aes(l2norm_ridge, l2norm, fill=R2.other.reg)) +
    ggtitle(title) +
    geom_vline(aes(xintercept=l2norm_ridge[which.max(R2.other.reg)]),
               linetype="dashed") +
    geom_abline(color="red") +
    geom_point(color="black",shape=21) +
    scale_y_log10(name=bquote(italic(l)^2~"penalty (GEV)"),
                  labels = scales::trans_format("log10", scales::math_format(10^.x))) +
    scale_x_log10(name=bquote(italic(l)^2~"penalty (Ridge)"),
                  labels = scales::trans_format("log10", scales::math_format(10^.x))) +
    scale_fill_scico(palette="batlow",name=bquote(R^2~"Ridge"))
  
  ## Save plot to disk:
  dir.create(plotpath_gev_eval <- file.path(plotpath_gev,"2_l2norm"),
             showWarnings=F, recursive=T)
  save_plot(scatterplot,
            file.path(plotpath_gev_eval,
                      paste("2_l2norm",model_train,loc_sname,sep="_")),
            width=5,height=4)
}


## Plot coefficient maps and prediction scatterplots for different lambda values:
plot_lambda_coefmap_predscat_gev <- function(res_gev_df_reg, name_y) {

  
  plot_lambda_label <- label_bquote(cols={lambda==10^.(base::round(log10(as.numeric(lambda)))) })
  
  eval_plot_df  <- res_gev_df_reg$eval %>% filter(lambda %in% lambda_exp_GEV_plot)
  pred_plot_df  <- res_gev_df_reg$pred %>% filter(lambda %in% lambda_exp_GEV_plot)
  coef_plot_df  <- res_gev_df_reg$coef %>%
    filter(lambda %in% lambda_exp_GEV_plot, !is.na(lon)) %>%
    group_by(lambda,model_train) %>%
    summarise(scale_coef=as.numeric(scale(coef_gev)), coord, lon, lat) %>%
    select(model_train, lambda, coord, scale_coef, lon, lat)
  
  df_reg_vec <- coef_plot_df %>% select(lambda) %>% distinct()
  coef_plot_df$scale_coef[coef_plot_df$scale_coef>  4] <-  4
  coef_plot_df$scale_coef[coef_plot_df$scale_coef< -4] <- -4
  
  ## Plot coefficient maps:
  map_coef_plot <- basis_maps[[loc_sname]]$map +
    ggtitle(location_df$loc_lname[location_df$loc_sname==loc_sname]) +
    geom_tile(data=coef_plot_df, aes(x=lon,y=lat,fill=scale_coef)) +
    facet_grid(model_train~lambda, labeller=plot_lambda_label) +
    borders("world",lwd=0.2,col="black") +
    basis_maps[[loc_sname]]$rect +
    scale_fill_gradient2(midpoint = 0,
                         low = brewer.pal(5,"BrBG")[5],mid = "white",high = muted("red"),
                         name=bquote("Scaled Estimates"~widehat(mu)["Z,i"])) +
    theme(legend.position = "right", legend.key.height = unit(1, "cm"))
  
  ## Plot prediction scatterplots:
  pred_plot_df$y.pred.minusnonreg <- pred_plot_df[,name_y]
  range_y     <- range(pred_plot_df$mu, pred_plot_df$y.pred.minusnonreg)
  pred_y_plot <- ggplot(pred_plot_df, aes(x=y.pred.minusnonreg, y=mu)) +
    facet_grid(model_train~lambda, labeller=plot_lambda_label) +
    scale_x_continuous(name="Residuals (no Z500 predictor)",limits=range_y) +
    scale_y_continuous(name="Predictions (with Z500 predictor)",limits=range_y) +
    geom_bin2d() +
    geom_abline(intercept=0, slope=1, col="red", linetype="dashed") +
    scale_fill_continuous(name=bquote("Count"), trans = "log10", guide = "colourbar",
                          breaks=10^(0:3),type = "viridis") +
    theme(legend.position = "right", legend.key.height = unit(1, "cm"))
  lambda_plots <- ggarrange(map_coef_plot, pred_y_plot, nrow = 2, align = "v")
  
  ## Combine plots and save those:
  # dir.create((plotpath_sample <- file.path(plotpath_ridge, "1_lambda_sample_coefmap_predscat")),
  #            showWarnings = F, recursive = T)
  save_plot(lambda_plots,
            file.path(plotpath_gev_checkplots,paste("1_lambda_coefmap_predscat",loc_sname,sep="_")),
            width=9,height=8)
}

## Plotting error measures as function of lambda:
plot_lambda_errormeas_gev <- function(res_gev_df_reg) {
  lambda_base_plot <- ggplot(res_gev_df_reg$eval,
                             aes(x=lambda,col=model_train)) + 
    geom_hline(yintercept=0, col="grey20") +
    scale_color_manual(values=modelcol,name="Model") +
    scale_x_log10(name=bquote("Regularisation paramter"~lambda),
                  breaks = range(lambda_exp_GEV_plot),
                  labels = scales::trans_format("log10", scales::math_format(10^.x)))
  range_lambda_x <- range(lambda_exp_GEV_plot)*10^c(-0.1,0.1)
  
  ## R2 Plot:
  opt_lambda_R2 <- res_gev_df_reg$eval %>% group_by(model_train) %>%
    summarise(max_R2  =max(R2.other.reg),
              lambda  =lambda[which.max(R2.other.reg)])
  R2_plot <- lambda_base_plot +
    geom_line(aes(y=R2.other.reg)) +
    geom_vline(data=opt_lambda_R2, aes(xintercept=lambda, col=model_train),
               linetype="dashed", alpha=0.5) +
    geom_point(data=opt_lambda_R2, aes(y=max_R2, col=model_train)) +
    coord_cartesian(xlim=range_lambda_x, expand = 0,
                    ylim=c(-0.1,1.1*max(res_gev_df_reg$eval$R2.other.reg))) +
    scale_y_continuous(name=bquote("Coefficient of determination"~R^2))
  
  ## RMSE Plot:
  opt_lambda_RMSE <- res_gev_df_reg$eval %>% group_by(model_train) %>%
    summarise(min_RMSE=min(RMSE),
              lambda  =lambda[which.min(RMSE)])
  ymax_RMSE <- res_gev_df_reg$eval %>% filter(lambda==max(.$lambda)) %>%
    pull(RMSE) %>% max()
  ymin_RMSE <- res_gev_df_reg$eval %>% pull(RMSE) %>% min()
  RMSE_plot <- lambda_base_plot +
    geom_line(aes(y=RMSE)) +
    geom_vline(data=opt_lambda_RMSE, aes(xintercept=lambda, col=model_train),
               linetype="dashed", alpha=0.5) +
    geom_point(data=opt_lambda_RMSE, aes(y=min_RMSE, col=model_train)) +
    coord_cartesian(xlim=range_lambda_x, expand = 0,
                    ylim=c(0.9*ymin_RMSE,1.1*ymax_RMSE)) +
    scale_y_continuous(name=bquote("RMSE"))
  
  ## Correlation Plot:
  opt_lambda_Corr <- res_gev_df_reg$eval %>% group_by(model_train) %>%
    summarise(max_Corr=max(Corr),
              lambda  =lambda[which.max(Corr)])
  range_corr <- res_gev_df_reg$eval %>% pull(Corr) %>% range()
  range_corr <- diff(range_corr)*c(-0.1,0.1)+range_corr
  Corr_plot <- lambda_base_plot +
    geom_line(aes(y=Corr)) +
    geom_vline(data=opt_lambda_Corr, aes(xintercept=lambda, col=model_train),
               linetype="dashed", alpha=0.5) +
    geom_point(data=opt_lambda_Corr, aes(y=max_Corr, col=model_train)) +
    coord_cartesian(xlim=range_lambda_x, expand = 0,
                    ylim=range_corr) +
    scale_y_continuous(name=bquote("Correlation"~r))
  
  res_plot <- ggarrange(RMSE_plot, Corr_plot, R2_plot, align = "v",
                        common.legend = T, legend = "bottom", ncol=1)
  save_plot(res_plot,
            file.path(plotpath_gev_checkplots,paste("2_ErrorMeasures_lambda",loc_sname,sep="_")),
            width=5,height=8)
  dev.off()
  
  ## Plot RMSE together with the maps:
  opt_lambda_R2$model_train  <- factor(opt_lambda_R2$model_train,
                                       levels=names(modelcol),ordered=T)
  maps_maxR2_df <- opt_lambda_R2 %>% 
    merge(., res_gev_df_reg$coef, by=c("lambda","model_train")) %>%
    filter(grepl("coord_",coord))
  
  res_plot_ridge_coef <- basis_maps[[loc_sname]]$map +
    geom_tile(data=maps_maxR2_df, aes(x=lon,y=lat,fill=coef_gev)) +
    facet_grid(~model_train) +
    basis_maps[[loc_sname]]$rect +
    #geom_polygon(data=gridbox3x3, aes(x=x,y=y), col="darkred", fill=NA) +
    borders("world",lwd=0.2,col="black") +
    colour_scale("loc_coef_map_fill") +
    theme(legend.position = "right", legend.key.height = unit(0.5, "cm"))
  
  
  res_plot <- ggarrange(R2_plot, res_plot_ridge_coef, nrow=2,
                        heights = c(0.65,0.35))
  save_plot(res_plot,
            file.path(plotpath_gev_checkplots,paste("2_R2_lambda_maps",loc_sname,sep="_")),
            width=10*3/4,height=6*3/4)
}


## Plotting predictors as function of lambda:
plot_lambda_predictors_gev <- function(res_gev_df_reg) {
  opt_lambda_R2 <- res_gev_df_reg$eval %>% group_by(model_train) %>%
    summarise(max_R2  =max(R2.other.reg),
              lambda  =lambda[which.max(R2.other.reg)])
  
  ncoef_nonreg <- length(unique(res_gev_df_reg$coef %>%
                                  filter(!grepl("coord_",coord)) %>%
                                  pull(coord)))
  
  ncoef_reg    <- length(unique(res_gev_df_reg$coef %>%
                                  filter(grepl("coord_",coord)) %>%
                                  pull(coord)))
  ## Make plots for non-regularised parameters:
  plot_coord_label <- label_bquote(cols=.(var_longname(coord)))
  
  res_plot <- res_gev_df_reg$coef %>% filter(!grepl("coord_",coord)) %>%
    ggplot(aes(x=lambda,col=model_train,y=coef_gev)) + 
    facet_wrap(~coord, scales = "free_y", labeller = plot_coord_label,nrow = 1) + 
    scale_color_manual(values=modelcol,name="Model") +
    scale_x_log10(name=bquote("Regularisation paramter"~lambda),
                  breaks = 10^(-5:8),
                  labels = scales::trans_format("log10", scales::math_format(10^.x))) +
    geom_line() +
    geom_vline(data=opt_lambda_R2, aes(xintercept=lambda, col=model_train),
               linetype="dashed", alpha=0.5) +
    scale_y_continuous(name=bquote("Estimated coefficient"~widehat(beta)[i])) +
    theme(legend.position = "bottom")
  
  save_plot(res_plot,
            file.path(plotpath_gev_checkplots,paste("3_Predictors_nonreg_lambda",loc_sname,sep="_")),
            width=ncoef_nonreg*3.3,height=4)
  
  ## Plot spaghetti-plots for regularised parameter:
  res_gev_df_reg$coef$model_train <- factor(res_gev_df_reg$coef$model_train,
                                          levels=names(modelcol),ordered=T)
  res_gev_df_reg$coef$coord    <- factor(res_gev_df_reg$coef$coord)
  opt_lambda_R2$model_train  <- factor(opt_lambda_R2$model_train,
                                       levels=names(modelcol),ordered=T)
  
  res_plot_reg <- res_gev_df_reg$coef %>% filter(grepl("coord_",coord)) %>%
    ggplot(aes(x=lambda, y=coef_gev, col=coord)) +
    facet_wrap(facets = ~model_train,ncol=1) +
    scale_x_log10(name=bquote("Regularisation paramter"~lambda),
                  breaks = 10^(-5:8),
                  # breaks = scales::trans_breaks("log10", function(x) 10^x),
                  labels = scales::trans_format("log10", scales::math_format(10^.x))) +
    scale_y_continuous(name=bquote("Estimates"~widehat(mu)["Z,i"])) +
    geom_line() +
    geom_vline(data=opt_lambda_R2, aes(xintercept=lambda),
               col=modelcol[as.integer(opt_lambda_R2$model_train)],
               linetype="dashed", alpha=0.9) +
    scale_color_manual(values=c(rep(brewer.pal(12,"Set3"),floor(ncoef_reg/12)),
                                brewer.pal(ncoef_reg%%12,"Set3"))[1:ncoef_reg],
                       guide="none") +
    coord_cartesian(xlim=range(lambda_exp_GEV_plot), expand = 0, ylim=c(-0.3,0.3))
  save_plot(res_plot_reg,
            file.path(plotpath_gev_checkplots,paste("3_Predictors_reg_lambda",loc_sname,sep="_")),
            width=5,height=7)
}

## Function returning singular values:
get_sing_val <- function(model_i, data_scale_transient_df, opt_lambda_R2, Q_mat) {
  lambda_i   <- opt_lambda_R2 %>% filter(model_train==model_i) %>% pull(lambda)
  k_i        <- opt_lambda_R2 %>% filter(model_train==model_i) %>% pull(df) -
    (length(col_X$ridge)-length(col_X$reg)) - as.numeric(use_intercept) - 1
  D <- data_scale_transient_df %>% filter(model==model_i) %>%
    select(col_X$reg) %>% data.matrix() %>% svd() %>% .$d %>% diag()
  
  sing_val_ls    <- diag(solve(t(D) %*% D) %*% t(D))
  sing_val_ridge <- diag(solve(t(D) %*% D + lambda_i*Q_mat[1:ncol(D),1:ncol(D)]) %*% t(D))
  sing_val_pcr   <- diag(solve(t(D[,1:k_i]) %*% D[,1:k_i]) %*% t(D[,1:k_i]))
  
  ## Get Eigenvalue ratios ridge/least_squares:
  ev_ratio_ridge <- sing_val_ridge/sing_val_ls
  
  ## Get Eigenvalue ratios PCR/least_squares:
  ev_ratio_pcr <- rep(0,length(sing_val_ls))
  ev_ratio_pcr[1:round(k_i)] <- 1
  
  ## Make data.frame and return:
  df_plot <- data.frame(y=c(ev_ratio_ridge,ev_ratio_pcr),
                        x=rep(1:length(ev_ratio_ridge), 2),
                        sing_val=rep(c("Ridge","PCR"),each=length(ev_ratio_ridge)),
                        model_train=model_i)
  return(df_plot)
}


## Plot cross-correlation in non-regularised parameters:
plot_coef_crosscorr <- function(res_gev_df) {
  models_fit <- names(model_ls)
  models_fit <- models_fit[models_fit!="ERA5"]
  cor_variables <- c("mu0","GMST_smooth_ANO","mrsol_STD","phi0","loc","shape")
  res_cor_ls <- lapply(models_fit, get_crosscorr,
                       res_gev_df$all_covar$opt_lambda$ci$coef,
                       cor_variables)
  res_cor_df <- do.call(rbind,res_cor_ls)
  
  cor_variables_sname <- lapply(cor_variables, par_shortname)
  plot_cor <- res_cor_df %>% pivot_longer(cor_variables, names_to = "cor_x") %>%
    ggplot(aes(x=cor_x, y=cor_y, fill=value)) +
    facet_grid(~model_train) +
    geom_tile(col="grey50") +
    # coord_cartesian(expand = 0) +
    scale_fill_gradient2(limits=c(-1,1), name="Correlation  ") +
    scale_x_discrete(labels=do.call(expression,cor_variables_sname)) +
    scale_y_discrete(labels=rev(do.call(expression,cor_variables_sname)), limits=rev) +
    coord_equal(expand = 0) +
    theme(axis.title = element_blank(), legend.position = "bottom",
          legend.key.width = unit(1.3,"cm"), legend.key.height = unit(0.2,"cm"),
          axis.text.x = element_text(angle=70, hjust = 1))
  save_plot(plot_cor, file = file.path(plotpath_loc,"4_coef_covar"),
            height = 4, width = 10)
}

## Compare SM parameters from linear model:
plot_Tx7d_SM_est <- function(res_gev_df) {
  levels_type <- c("GEV All covar.","LinReg SM only")
  models_fit <- names(model_ls)
  models_fit <- models_fit[models_fit!="ERA5"]
  
  SM_est_all_df <- res_gev_df$all_covar$opt_lambda$coef %>%
    filter(coord %in% c("mrsol_STD")) %>%
    select(coord, model_train, coef_gev, cil99, ciu99) %>%
    mutate(type=factor("GEV All covar.",levels=levels_type,ordered=T))
  
  SM_est_lm_df <- data_scale_df %>% filter(exp!="piControl" &  year>1900) %>%
    group_by(model) %>%
    mutate(Tx7d_degC_GMSTcorr = residuals(lm(Tx7d_degC~GMST_smooth_ANO))) %>%
    summarise(coef_gev = coefficients(lm(Tx7d_degC_GMSTcorr~mrsol_STD))[2],
              cil99 = confint(lm(Tx7d_degC_GMSTcorr~mrsol_STD),level=0.99)[2,1],
              ciu99 = confint(lm(Tx7d_degC_GMSTcorr~mrsol_STD),level=0.99)[2,2]) %>%
    mutate(type=factor("LinReg SM only",levels=levels_type,ordered=T),
           coord="mrsol_STD") %>%
    rename(model_train=model)

  SM_est_df   <- rbind(SM_est_all_df, SM_est_lm_df)
  SM_est_df <- SM_est_df %>%
    mutate(model_train=factor(model_train, levels=names(modelcol), ordered=T))
  SM_est_plot <- ggplot(SM_est_df, aes(x=type, y=coef_gev, color=model_train)) +
    geom_point() + #geom_line(type="dashed") +
    geom_errorbar(aes(ymin=cil99, ymax=ciu99), width=0.25) +
    facet_grid(~model_train) +
    scale_color_manual(values=modelcol) +
    scale_y_continuous(name=bquote("SM param."~widehat(mu)[SM]~"[°C "*sd^-1*"]")) +
    theme(legend.position = "none", axis.title.x = element_blank(),
          axis.text.x = element_text(angle=70, hjust = 1))
  save_plot(SM_est_plot, file = file.path(plotpath_loc,"5_Est_SM"),
            height = 3, width = 5)
}

## Compare GMST parameters from different models:
plot_Tx7d_GMST_est <- function(res_gev_df) {
  levels_type <- c("GEV All covar.","GEV GMST only","LinReg GMST only")
  models_fit <- names(model_ls)
  models_fit <- models_fit[models_fit!="ERA5"]
  
  GMST_est_all_df <- res_gev_df$all_covar$opt_lambda$coef %>%
    filter(coord %in% c("GMST_smooth_ANO")) %>%
    select(coord, model_train, coef_gev, cil99, ciu99) %>%
    mutate(type=factor("GEV All covar.",levels=levels_type,ordered=T))
  
  
  # GMST_est_df <- res_gev_df$GMST_covar$par %>%
  #   select(GMST_smooth_ANO,model_train) %>%
  #   rename(coef_gev=GMST_smooth_ANO) %>%
  #   mutate(coord=GMST_smooth_ANO)
  
  GMST_est_fevd_df <- t(sapply(models_fit,get_est_ci_fevd,
                               res_gev_df$GMST_covar$full_model))
  colnames(GMST_est_fevd_df) <- c("cil99", "coef_gev", "ciu99")
  GMST_est_GMST_df <- GMST_est_fevd_df %>% data.frame() %>%
    mutate(model_train=rownames(.), coord="GMST_smooth_ANO",
           type=factor("GEV GMST only",levels=levels_type,ordered=T))
  
  
  GMST_est_lm_df <- data_scale_df %>% filter(exp!="piControl" &  year>1900) %>%
    group_by(model) %>%
    summarise(coef_gev = coefficients(lm(Tx7d_degC~GMST_smooth_ANO))[2],
              cil99 = confint(lm(Tx7d_degC~GMST_smooth_ANO),level=0.99)[2,1],
              ciu99 = confint(lm(Tx7d_degC~GMST_smooth_ANO),level=0.99)[2,2]) %>%
    mutate(type=factor("LinReg GMST only",levels=levels_type,ordered=T),
           coord="GMST_smooth_ANO") %>%
    rename(model_train=model)
  
  tasseas_GMST_coef <- VAR_df_1d %>% filter(location==loc_sname & model!="ERA5") %>%
    mutate(tas_DIFF_LOC=tas_ABS_LOC-tas_ANO_LOC) %>%
    group_by(model) %>%
    summarise(tasseas_GMST_coef=coefficients(lm(tas_DIFF_LOC~GMST_smooth_ANO))[2],
              tasseas_GMST_cil99=confint(lm(tas_DIFF_LOC~GMST),level=0.99)[2,1],
              tasseas_GMST_ciu99=confint(lm(tas_DIFF_LOC~GMST),level=0.99)[2,1]) %>%
    rename(model_train=model)
  
  GMST_est_df   <- rbind(GMST_est_GMST_df, GMST_est_all_df, GMST_est_lm_df)
  GMST_est_df <- GMST_est_df %>%
    mutate(model_train=factor(model_train, levels=names(modelcol), ordered=T))
  GMST_est_plot <- ggplot(GMST_est_df, aes(x=type, y=coef_gev, color=model_train)) +
    geom_point() + #geom_line(type="dashed") +
    geom_errorbar(aes(ymin=cil99, ymax=ciu99), width=0.25) +
    facet_grid(~model_train) +
    scale_color_manual(values=modelcol) +
    scale_y_continuous(name=bquote("GMST param."~widehat(mu)[GMST]~"[°C °"*C^-1*"]")) +
    theme(legend.position = "none", axis.title.x = element_blank(),
          axis.text.x = element_text(angle=70, hjust = 1)) +
    geom_hline(data=tasseas_GMST_coef, aes(yintercept=tasseas_GMST_coef,
                                           color=model_train))
  save_plot(GMST_est_plot, file = file.path(plotpath_loc,"5_Est_GMST"),
            height = 3, width = 5)
}

## Wrapping function for the plots above:
plot_GEV_fitting <- function(res_gev_df, name_y) {
  ## Plot regularised coefficient maps:
  plot_lambda_coefmap_predscat_gev(res_gev_df$all_covar, name_y)
  plot_lambda_errormeas_gev(res_gev_df$all_covar)
  plot_lambda_predictors_gev(res_gev_df$all_covar)
  plot_coef_crosscorr(res_gev_df)
  plot_Tx7d_GMST_est(res_gev_df)
  plot_Tx7d_SM_est(res_gev_df)
}

## Plot comparison of lambda and degrees-of-freedom for optimal lambdas:
# plot_lambda_df_opt <- function(res_ridge_df, data_df_plot) {
#   opt_lambda_R2 <- res_ridge_df$eval %>% group_by(model_train) %>%
#     summarise(max_R2  =max(R2.other.reg),
#               lambda  =lambda[which.max(R2.other.reg)],
#               df      =df[which.max(R2.other.reg)])
#   opt_lambda_R2_coef <- merge(res_ridge_df$coef,opt_lambda_R2,
#                               by=c("lambda","model_train","df"))
#   sing_val_ls <- lapply(unique(res_ridge_df$eval$model_train), get_sing_val,
#                         data_df_plot, opt_lambda_R2, Q_mat)
#   sing_val_df <- do.call(rbind,sing_val_ls)
#   
#   sing_val_df$sing_val <- factor(sing_val_df$sing_val,levels=c("Ridge","PCR"),
#                                  ordered=T)
#   plot_eigenvalratio <- ggplot(sing_val_df, aes(x,y,col=model_train,linetype=sing_val)) +
#     geom_step() +
#     scale_linetype_discrete(name="Modelling\napproach") +
#     scale_color_manual(values=modelcol,name="Model") +
#     scale_x_log10(name="Eigenvalue index j") +
#     scale_y_continuous(name="Eigenvalue ratio (regularised/least squares)")
#   save_plot(plot_eigenvalratio,
#             file.path(plotpath_ridge,paste("4_Eigenval_Ratios_",loc_sname,sep="_")),
#             width=7,height=5)
# }


## Function to make ridge plots:
# make_ridge_plots_GEV <- function(res_ridge_df, data_scale_pictrl_df, name_y) {
#   
#   loc_sname <- unique(res_ridge_df$pred$location)
#   dir.create(plotpath_ridge <<- file.path(plotpath,"1_ridge",loc_sname),
#              showWarnings = F, recursive = T)
#   
#   ## Plot samples of coef-maps and the corresponding predictions:
#   plot_lambda_coefmap_predscat(res_ridge_df, name_y=name_y)
#   
#   ## Plot different error measures as function of lambda:
#   plot_lambda_errormeas(res_ridge_df)
#   
#   ## Plot different additional predictors as function of lambda:
#   plot_lambda_predictors(res_ridge_df)
# }