### README for the repository of the "Nonstationary Extremes" Project

## What sort of code is this?

This is the code used for the publication "Quantifying the statistical dependence of mid-latitude heatwave intensity and likelihood on prevalent physical drivers and climate change" in ASCMO (Advances in Statistical Climatology, Meteorology and Oceanography) by Joel Zeder and Erich M. Fischer (_insert ASCMO DOI here_).

## How to use this repo:

If you intend to reproduce the results of this paper, or if you want to use the code for further research, be aware of the following: The code requires quite a lot of pre-processed data, which is made available as .csv files on the ETH research collection (the original NetCDF files might be available upon request, and was pre-processed with the scripts numbered 1-3). Also, the scripts might need adjustments (in terms of paths that need to be specified, etc.). In general, follow these steps:
- Download the pre-processed data from _insert research collection DOI here_
- Import the data with the code in the script ```3_ReadWrite_csv.R```
- For explanatory data analysis, run ```4_EDA.R```
- For the statistical modelling part (obtaining starting values via ridge regression, and for fitting the regularised GEV distributions), run ```5_StatModelling.R```
- For inspecting the fitted models, run ```6_StatAnalysis_ParameterAnalysis.R```
- For applying the model to specific heatwave events, run ```6_StatAnalysis_EventAnalysis.R```

## How to contact in case of questions?

The main author of this code is Joel (info@joel-zeder.ch), if not reachable please contact Erich (erich.fischer@env.ethz.ch).

## Session Info:

This is information on the R session, which was used to run the scripts:

```
[1] "2022-03-09"
> sessionInfo()
R version 4.2.2 (2022-10-31)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: openSUSE Leap 15.4

Matrix products: default
BLAS/LAPACK: /usr/local/OpenBLAS-0.3.21/lib/libopenblas_haswellp-r0.3.21.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C               LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8     LC_MONETARY=en_US.UTF-8   
 [6] LC_MESSAGES=en_US.UTF-8    LC_PAPER=en_US.UTF-8       LC_NAME=C                  LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] tools     parallel  stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] ncdf4_1.19          rnaturalearth_0.1.0 sf_1.0-9            metR_0.13.0         extRemes_2.1-3      distillery_1.2-1   
 [7] Lmoments_1.3-1      scico_1.3.1         geosphere_1.5-18    RColorBrewer_1.1-3  broom_1.0.1         scales_1.2.1       
[13] ggpubr_0.5.0        ggplot2_3.4.0       tidyr_1.2.1         dplyr_1.0.10        MASS_7.3-58.1      

loaded via a namespace (and not attached):
 [1] tidyselect_1.2.0   purrr_0.3.5        lattice_0.20-45    carData_3.0-5      colorspace_2.0-3   vctrs_0.5.1        generics_0.1.3    
 [8] utf8_1.2.2         rlang_1.0.6        e1071_1.7-12       pillar_1.8.1       glue_1.6.2         withr_2.5.0        DBI_1.1.3         
[15] sp_1.5-1           lifecycle_1.0.3    munsell_0.5.0      ggsignif_0.6.4     gtable_0.3.1       memoise_2.0.1      fastmap_1.1.0     
[22] class_7.3-20       fansi_1.0.3        evd_2.3-6.1        Rcpp_1.0.9         KernSmooth_2.23-20 backports_1.4.1    classInt_0.4-8    
[29] checkmate_2.1.0    cachem_1.0.6       abind_1.4-5        rstatix_0.7.1      grid_4.2.2         rgdal_1.6-2        cli_3.4.1         
[36] maps_3.4.1         magrittr_2.0.3     proxy_0.4-27       tibble_3.1.8       car_3.1-1          pkgconfig_2.0.3    data.table_1.14.6 
[43] assertthat_0.2.1   rstudioapi_0.14    R6_2.5.1           units_0.8-0        compiler_4.2.2  
```